<?php
/*
Template Name: Home Participación
*/
/// Set the slider
?>
<?php get_header(); ?>
<div class="main-modulo_colaboracion">

<!-- STARTS STEPS FOR PROYECT -->
<?php $array_featured = get_field('cabecera_participacion','options'); ?>
<?php if( $array_featured ): ?>
	<div class="colaboracion_pasos">
		<?php foreach( $array_featured as $img): ?>

		<div class="colaboracion_pasos_all">
			<img src="<?php echo $img['sizes']['hparticipacion']; ?>" alt="<?php echo $img['alt']; ?>" class="" />
		</div>
		
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<!-- ENDS STEPS FOR PROYECT -->

<!-- STARTS DIVISOR FOR PROYECT -->
<section class="homeColab-sectionDivisor">
	<figure class="homeColab-tittleDivisor">
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/title-divisor.jpg" alt="divisor">	
	</figure>

</section>

<!-- ENDS DIVISOR FOR PROYECT -->



<!-- STARTS FEATURE PROYECT -->
	
    	<div class="colaboracion_proyecto-destacado">
    		
        		<a href="#" title="Proyecto Destacado 1" >
				<div class="titleProyectoDestacado">
					<h2>Proyecto Destacado 1</h2>
					<p class="icon-star-full"></p>
				</div>
			
				<div class="imgProyectoDestacado">
					<figure class="homeParticipa-featureImg">
						<img src="<?php echo site_url() ?>/wp-content/uploads/2016/03/proyecto-libertadores.jpg" alt="">
					</figure>
				</div>
				</a>
				
		</div>

<!-- ENDS FEATURE PROYECT -->

	
<!-- STARTS BUTTON FOR PROYECT -->
	
	<button class="homeParticipa_btn homeParticipa_orange" type="button">
		<a style="text-decoration:none;" href="<?php echo site_url() ?>/formulario"><span><h1 style="color:white;">Participa con una Propuesta</h1></a>
		</span>
	</button>

<!-- ENDS BUTTON FOR PROYECT -->

	
	<div class="mainPropuestas">
        	

<!-- START PRIMERA PROPUESTA -->
			
    <?php
        $args = array(
                    'post_type' => 'proyecto',
                    'post_status' => 'publish',
                    'posts_per_page' => 6
                );

        $the_query = new WP_Query( $args );
    ?>


    <?php if ( $the_query->have_posts() ) { ?>

        <div class="homeParticipa-wrapper" id="post_cards">

        <?php while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
    		
            <div class="card radius shadowDepth1" >
    			<div class="card__image border-tlr-radius">
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('last', array('class' => 'img-responsive border-tlr-radius')); ?></a>
                </div>

    			<div class="card__content card__padding">
                    <div class="card__share">
                        <div class="card__social">  
                            <a class="share-icon facebook" href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php the_permalink(); ?>&p[title]=<?php the_title(); ?>&p[summary]=<?php the_excerpt(); ?>&p[images][0]=<?php get_the_post_thumbnail( $post_id, 'thumbnail' ); ?>"><span class="fa fa-facebook"></span></a>
                            <a class="share-icon twitter" href="http://twitter.com/home?status=Mira%20esta%20propuesta%20para%20San%20Isidro%20<?php the_permalink(); ?>"><span class="fa fa-twitter"></span></a>
                            <a class="share-icon googleplus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><span class="fa fa-google-plus"></span></a>
                        </div>

                        <a id="share" class="share-toggle share-icon" href="#"></a>
                    </div>

    				<div class="card__meta">
                        <?php the_terms( get_the_ID(), 'tema', ' ', ', ',' ' ); ?>
                        <?php the_time('j M', '<time>', '</time>'); ?>
    				</div>

    				<article class="card__article">
	    				<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" ><?php the_title(); ?></a></h2>
	    			</article>
    			</div>
    		</div>
        <?php } ?>

        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query(); ?>
        
        </div>
        <div id="morecards_loading" style="display:none;"><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="loading" /></div>
    <?php } ?>
	<!-- END Primera Propuesta -->

	<button class="homeParticipa_btn homeParticipa_orange" type="button" id="button_more_cards">
		<span><h1 style="color:white;">Ver mas Propuestas</h1>
		</span>
	</button>
	

<?php get_footer(); ?>