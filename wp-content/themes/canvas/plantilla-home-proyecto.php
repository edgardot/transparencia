	
<?php
/*
Template Name: Home Gobierno Abierto
*/

?>

<?php get_header(); ?>
<!--STAR CUSTOM CODE-->


<section class="gobAb-main">
	
	
	<!--IMG PRINCIPAL DESCRIPTION-->

	<section class="gobAb-description">
		
		<article class="gobAb-description-text">
			
			<div class="gobAb-description-text-h1">
				<h1 class="gobAb-description-text-h1-text">PORTAL DE GOBIERNO ABIERTO</h1>
			</div>
			
			<div class="gobAb-description-text-p">
				<p class="gobAb-description-text-p1">
					Un gobierno abierto promueve la transparencia, la participación ciudadana y la colaboración
					en la gestión pública. Un gobierno municipal abierto rinde cuenta permanentemente a la 
					ciudadanía y brinda facilidades para que sus vecinos puedan fiscalizar el buen uso de los recursos.
					Una municipalidad abierta invita a sus vecinos a participar en la definición de las 
					 políticas públicas y fomenta la colaboración de la ciudadanía, empresas y organizaciones para 
					 mejorar los servicios que brinda.
					</p>
				<!-- <p class="gobAb-description-text-p2">
					Presentamos este portal de gobierno abierto en el que nuestros vecinos podrán participar 
					proponiendo ideas y votar por las de su preferencia, además de conocer de una manera 
					transparente como se viene ejecutando el presupuesto municipal.
				</p> -->

				<p>Este portal ha sido realizado por el equipo Mr. Roboto integrado por <a href="https://twitter.com/emtv" target="_blank">Edgardo Tupiño</a> 
					y <a href="https://twitter.com/agonzalesc" target="_blank">Alexander Gonzales</a>, 
					ganadores de la <a href="http://msi.gob.pe/portal/innovacion/hackathon-san-isidro/" target="_blank">Primera Hackaton San Isidro</a>, realizada el 12 de diciembre de 2015 y por ello, 
					es un ejemplo de colaboración ciudadana.
					</p>

				<p>Los invitamos a conocer las actividades que realizamos para promover la innovación en 
					la gestión municipal: <a href="http://msi.gob.pe/portal/innovacion/">http://msi.gob.pe/portal/innovacion/</a>
					</p>
			</div>
			

		</article>

	</section>


	<!--VISIBLE FOR MOBILES -->
		<div class="gobAb-mobile">
<!-- 			<figure class="gobAb-mobile-col">
				<a href="<?php echo site_url() ?>/participacion">	
					<img class="gobAb-mobile-col-img" src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/mobile-col.jpg" alt="">
				</a>	
			</figure> -->
			<figure class="gobAb-mobile-tra">
				<a href="<?php echo site_url() ?>/transparencia">	
					<img class="gobAb-mobile-tra-img" src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/mobile-tra.jpg" alt="">
				</a>	
			</figure>

		</div>
	
	<!--VISIBLE FOR DESKTOP -->
		<div class="gobAb-desktop">
<!-- 			<figure class="gobAb-desktop-col">
				<a href="<?php echo site_url() ?>/participacion">	
					<img class="gobAb-desktop-col-img" src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/desktop-col.jpg" alt="">
				</a>	
			</figure> -->
			<figure class="gobAb-desktop-tra">
				<a href="<?php echo site_url() ?>/transparencia">	
					<img class="gobAb-desktop-tra-img" src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/banner-transparencia-home.jpg" alt="">
				</a>	
			</figure>

		</div>


</section>

<!--FINISH CUSTOM CODE-->
<?php get_footer(); ?>