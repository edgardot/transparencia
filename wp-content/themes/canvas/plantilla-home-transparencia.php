	
<?php
/*
Template Name: Home Transparencia
*/

?>

<?php get_header(); ?>
<!--STAR CUSTOM CODE-->


<div class="homeTransparencia">

<!-- STARTS STEPS FOR PROYECT -->
	<div class="colaboracion_pasos">
		<div class="transparencia_banner1-2">
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/banner_1-2.jpg" alt="">
		</div>
	

<!-- 		<div class="colaboracion_pasos-1">
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/col1.jpg" alt="Paso 1">
		</div>
		<div class="colaboracion_pasos-2">
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/col2.jpg" alt="Paso 2">
		</div>
		<div class="colaboracion_pasos-3">
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/col3.jpg" alt="Paso 3">
		</div> -->
	</div>

<!-- ENDS STEPS FOR PROYECT -->

<!-- STARTS DIVISOR FOR PROYECT -->
	<section class="homeColab-sectionDivisor">
		<figure class="homeColab-tittleDivisor">
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/title-divisor-pre.jpg" alt="divisor">	
		</figure>

	</section>

<!-- ENDS DIVISOR FOR PROYECT -->

	<section class="ht-sections-desktop">

		<figure>
			<a href="<?php echo site_url() ?>/presupuesto-anual">
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/banner-desktop-anual.jpg" alt="">
			</a>	
		</figure>
		<figure>
			<a href="<?php echo site_url() ?>/presupuesto-mensual">
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/banner-desktop-mensual.jpg" alt="">
			</a>	
		</figure>
		<figure>
			<a href="<?php echo site_url() ?>/detalle">
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/banner-desktop-categoria.jpg" alt="">
			</a>	
		</figure>
		

	</section>

	<section class="ht-sections-mobile">

		<figure>
			<a href="<?php echo site_url() ?>/presupuesto-anual">
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/banner-mobile-anual.jpg" alt="">

			</a>	
		</figure>
		<figure>
			<a href="<?php echo site_url() ?>/presupuesto-mensual">
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/banner-mobile-mensual.jpg" alt="">
			</a>
		</figure>
		<figure>
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/banner-mobile-categoria.jpg" alt="">
		</figure>
		

	</section>	

	<div class="ht-divisor2">
		<figure class="ht-divisor2-img">
			<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/title-divisor-cat.jpg" alt="divisor">	
		</figure>
	</div>

	
	<section class="ht-conceptos">
		
		<article class="ht-conceptos-article">
			<div class="ht-conceptos-article-head">
				<h1 class="ht-colorText">FUNCIÓN</h1>
				<p class="ht-colorText-p">El gasto público se divide en clasificadores programáticos para fines estadísticos, 
					de estudio y sistematización. Estos clasificadores son aprobados por el Ministerio 
					de Economía y Finanzas y son comunes a todas las entidades públicas. La “función” 
					es uno de estos clasificadores y corresponde al cumplimiento de los deberes 
					constitucionales.</p>
			</div>

			<div class="ht-conceptos-article-pia-pim">
				<div class="ht-pia">
					<h1 class="ht-colorText">PIA</h1>
					<p class="ht-colorText-p">Presupuesto Institucional de Apertura, es el presupuesto inicial de la entidad pública 
						aprobado por su respectivo Titular con cargo a los créditos presupuestarios establecidos 
						de acuerdo a ley. Se aprueba en diciembre.</p>
				</div>
				<div class="ht-pim">
					<h1 class="ht-colorText">PIM</h1>
					<p class="ht-colorText-p">Presupuesto Institucional Modificado, es el presupuesto actualizado de la entidad pública a 
						consecuencia de las modificaciones presupuestarias, tanto a nivel institucional como a nivel 
						funcional programático, efectuadas durante el año fiscal, a partir del PIA.</p>
				</div>
			</div>


		</article>


<!-- 		<figure class="ht-conceptos-figure">
			<img class="ht-conceptos-img" src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/conceptos.jpg" alt="">
		</figure> -->

	</section>



	<section class="ht-categories">

		<figure class="ht-imgCategory">
			
			<a href="<?php echo site_url() ?>/planeamiento">
				<article class="article-div-tra"><p>Acciones realizadas para la dirección y correcta conduccion de las politicas de gobierno</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-planeamiento.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/seguridad">
				<article class="article-div-tra"><p>Acciones necesarias para garantizar y presenvar el orden público</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-seguridad.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/comercio">
				<article class="article-div-tra"><p>Gestiones orientadas a la promoción del comercio interno y externo</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-comercio.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/transporte">
				<article class="article-div-tra"><p>Acciones para mejorar los diversos medios de transporte que se usan en un territorio</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-transporte.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/ambiente">
				<article class="article-div-tra"><p>Desarrollo estratégico, conservación y aprovechamiento sostenible del patrimonio natural</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-ambiente.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/desarrollo-vivienda">
				<article class="article-div-tra"><p>Desarrollo de las actividades de vivienda, desarrollo urbano y edificaciones</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-vivienda.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/salud">
				<article class="article-div-tra"><p>Acciones y servicios orientados a mejorar el bienestar de la población.</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-salud.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/cultura">
				<article class="article-div-tra"><p>Acciones orientadas a desarrollar la calidad de vida del individuo y mejorar la convivencia social</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-cultura.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/proteccion-social">
				<article class="article-div-tra"><p>Acciones orientadas a mejorar el desarrollo social de los ciudadanos</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-proteccion.jpg" alt=""></figure>
			</a>	
		<figure class="ht-imgCategory">
			<a href="<?php echo site_url() ?>/prevision-social">
				<article class="article-div-tra"><p>Acciones desarrolladas para garantizar la cobertura financiera del pago y la asistencia a los asegurados</p></article>
				<img src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-prevision.jpg" alt=""></figure>
			</a>
		<figure class="ht-imgCategory ht-imgCategory-blank">
			<a href="#">
				
				<img style="cursor:default;" src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-blank.png" alt=""></figure>
			</a>		
		<figure class="ht-imgCategory ht-imgCategory-blank">
			<a href="#">
				
				<img style="cursor:default;" src="<?php echo site_url() ?>/wp-content/themes/canvas/assets/hometransparencia/cat/cat-blank.png" alt=""></figure>
			</a>	
		

	</section>






</div>


<!--FINISH CUSTOM CODE-->

<?php get_footer(); ?>