<?php get_header(); ?>
<div class="main-modulo_colaboracion">

<section class="taxonomyPage-section-h1">

    <h1 class="taxonomyPage-h1">PROPUESTAS POR CADA GERENCIA</h1>
    

</section>




<!-- STARTS BUTTON FOR PROYECT -->
	
	<button class="homeParticipa_btn homeParticipa_orange" type="button">
		<a style="text-decoration:none;" href="<?php echo site_url('/formulario'); ?>">
		<span>
			<h1 style="color:white;">Participa con una Propuesta</h1>
		</span>
		</a>
	</button>

<!-- ENDS BUTTON FOR PROYECT -->

	
	<div class="mainPropuestas">


    <?php if ( have_posts() ) { ?>

        <div class="homeParticipa-wrapper" id="post_cards">

        <?php while ( have_posts() ) { the_post(); ?>
    		
            <div class="card radius shadowDepth1" >
    			<div class="card__image border-tlr-radius">
                    <?php the_post_thumbnail('last', array('class' => 'img-responsive border-tlr-radius')); ?>
                </div>

    			<div class="card__content card__padding">
                    <div class="card__share icon-share-card">
                        <div class="card__social">  
                            <a class="share-icon facebook" href="#"><span class="fa fa-facebook"></span></a>
                            <a class="share-icon twitter" href="#"><span class="fa fa-twitter"></span></a>
                            <a class="share-icon googleplus" href="#"><span class="fa fa-google-plus"></span></a>
                        </div>

                        <a id="share" class="share-toggle share-icon" href="#"></a>
                    </div>

    				<div class="card__meta">
                        <?php the_terms( get_the_ID(), 'tema', ' ', ', ',' ' ); ?>
                        <?php the_time('j M', '<time>', '</time>'); ?>
    				</div>

    				<article class="card__article">
	    				<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" ><?php the_title(); ?></a></h2>
	    			</article>
    			</div>
    		</div>
        <?php } ?>

        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query(); ?>
        
        </div>

    <?php } ?>
	<!-- END Primera Propuesta -->
<?php get_footer(); ?>