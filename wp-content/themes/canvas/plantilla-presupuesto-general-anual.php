	
<?php
/*
Template Name: Presupuesto Anual
*/

?>

<?php get_header(); ?>
<!--STAR CUSTOM CODE-->

<h1>Presupuesto Anual</h1>

<section class="presupuestoSectionMain">
  
  <input class="presupuestotabInput" id="tab1" type="radio" name="tabs" checked>
  <label class="presupuestotabLabel icon-2015" for="tab1">2015</label>
    
  <input class="presupuestotabInput" id="tab2" type="radio" name="tabs">
  <label class="presupuestotabLabel icon-2015" for="tab2">2016</label>
    
  <input class="presupuestotabInput" id="tab3" type="radio" name="tabs">
  <label class="presupuestotabLabel icon-compara" for="tab3">Comparar</label>
    
  <section class="presupuestoSection" id="content1">
    
    <div id="anual2015"></div>
  
  </section>
    
  <section class="presupuestoSection" id="content2">
 
    <div id="anual2016"></div>

  </section>
    
  <section class="presupuestoSection" id="content3">
   
    <iframe class='highcharts-iframe' src='//cloud.highcharts.com/embed/iboxaq' style='border: 0; width: 100%; height: 500px'></iframe>

  </section>

    <div class="fuente-datos">
    <p>FUENTE: Estos datos han sido consultados desde el <a href="http://datosabiertos.msi.gob.pe/home">Portal de Datos Abieros de la Municipalidad de San Isidro</a></p>
  </div>
    
    
</section>


<!--FINISH CUSTOM CODE-->

<?php get_footer(); ?>