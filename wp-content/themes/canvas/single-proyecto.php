<?php get_header(); ?>
       
<div id="content" class="col-full">
	<section id="main">                       

	<?php if (have_posts()) { ?>
		<?php while (have_posts()) { the_post(); ?>

			<?php the_post_thumbnail('single', array('class' => 'img-responsive border-tlr-radius')); ?>

			<article <?php post_class(); ?>>
				<header>
					<?php the_title( '<h1 class="title">', '</h1>' ); ?>
				</header>
			
				<section class="entry proyectoSingle-participa">
				
				<div class="proyectoSingle-detalles">
					
						<div class="post-info" style="display:block;">Propuesto por <?php the_author(); ?> | <?php the_terms( $post->ID, 'tema', ' ', ', ',' ' ); ?> | <?php comments_popup_link('0 Colaboraciones','1  Colaboracion','% Colaboraciones','comments-link','Colaboraciones Cerradas'); ?> | <?php the_time('j F, Y') ?><?php the_field('expediente'); ?></div>

						<?php if( is_user_logged_in() ) { ?>
		  					<?php if($robot->can_vote(get_current_user_id(),get_the_ID())) { ?>
		  				
		  					<div class="votar">
		  						<a id="roboto-vote" data-post="<?php the_ID(); ?>" data-canvote="1" class="rd_normal_bt large_rd_bt bt_icon_right bt_icon_border">
		  						<span><?php the_field('numvotes'); ?> Voto(s)</span>
		    				</a>
		  				</div>
		  			<?php } else { ?>
		  				<div class="votar">
		  					<a id="roboto-vote" data-post="<?php the_ID(); ?>" data-canvote="0" class="rd_normal_bt large_rd_bt bt_icon_right bt_icon_border" >
			    				Ya Votaste
			    				<span><?php the_field('numvotes'); ?> Voto(s)</span> 
		    				</a>
		  				</div>
		  			<?php } ?>
		  		<?php } else { ?>
		  				<div class="votar">
		  					<a href="<?php echo site_url() ?>/login" class="btn btn-big" style="background: #2ecc71;border-radius: 5px;padding: 1.5em 4.5em;color: white;text-decoration: none;">
			    				Vota por esta Propuesta
			    				<!-- <span><?php the_field('numvotes'); ?> Voto(s)</span>  -->
		    				</a>
		  				</div>
		  		<?php } ?>
			</div>
						<!-- START MODAL LOGIN CODE -->

<!-- Modal LOGIN -->
<div class="modalMsi" id="modal-one" aria-hidden="true">

    
 <div class="modalMsi-dialog">
				  <a href="#closeMsi" class="btnMsi-close" aria-hidden="true">×</a>
   <div class="loginmodalMsi-container">
					  <h1>Accede con tu Cuenta</h1><br>
				    <form action="<?php bloginfo('url') ?>/wp-login.php" method="POST">
					      <input class="emailMsi" type="text" name="log" placeholder="Tu Correo">
					      <input class="passMsi" type="password" name="pwd" placeholder="Contraseña">

					      <?php do_action('login_form'); ?>
							<input type="submit" name="wp-submit" class="submitMsi login loginmodalMsi-submit" value="Entrar" tabindex="14" class="user-submit" />
							<input type="hidden" name="redirect_to" value="<?php echo site_url($_SERVER['REQUEST_URI'], 'login_post'); ?>" />
							<input type="hidden" name="user-cookie" value="1" />
				    </form>
					
				  <div class="loginMsi-help">
					    <a class="a-btn-registrate-modal" href="#modal-two"><div class="btn-registrate-modal">Registrate</div></a>
    <a class="a-btn-clave-modal" href="#">
      <div class="btn-clave-modal">Olvidaste tu Clave</div>
    </a>
				  </div>
				</div>
			</div>
  

    
  

</div>
<!-- /Modal LOGIN -->

<!-- Modal REGISTER -->
<div class="modalMsi" id="modal-two" aria-hidden="true">

    
 <div class="modalMsi-dialog">
				  <a href="#closeMsi" class="btnMsi-close" aria-hidden="true">×</a>
   <div class="loginmodalMsi-container">
					  <h1>Registrate</h1><br>
				    <form action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>" method="POST">
					      <input class="textMsi" type="text" name="register_name" placeholder="Tu Nombre" />
<!-- 					      <input class="textMsi" type="text" name="register_lastname" placeholder="Tu Apellido" /> -->
					      <input class="textMsi basictxt" tabindex="1" data-tool="DNI CUI" type="text" name="godni_text" placeholder="Tu DNI" id="godni_text" /><span class="fieldtip">Tu DNI+cui - seguido<br><br><img src="http://test.gopymes.pe/gobiernoabierto/wp-content/themes/canvas/assets/imgtooltip.jpg" alt="" /><br><br>Ejm. 427363273 | el 3 es el cui</span>
					      <input class="textMsi" type="text" name="register_telefono" placeholder="Tu Telefono" />
					      <!--input class="textMsi" type="text" name="user_login" placeholder="Tu Usuario" /-->
					      <input class="textMsi" type="text" name="user_email" placeholder="Tu Correo" />

					      <?php //do_action('register_form'); ?>
							<input type="submit" name="login" value="Registrate" class="submitMsi login loginmodalMsi-submit" tabindex="103" />
							<?php $register = $_GET['register']; if($register == true) { echo '<p>Check your email for the password!</p>'; } ?>
							<input type="hidden" name="redirect_to" value="<?php echo site_url($_SERVER['REQUEST_URI'], 'login_post'); ?>"
							<input type="hidden" name="user-cookie" value="1" />

						<a href="http://test.gopymes.pe/gobiernoabierto/wc-api/auth/facebook/?return=http%3A%2F%2Ftest.gopymes.pe%2Fgobiernoabierto%2Fcheckout%2F">
					      <button class="homeParticipa_btn homeParticipa_orange loginFacebookMsi" type="button"><h2>Entra con Facebook</h2></button>
					      <!-- <input type="submit" name="login" class="submitMsi registerFacebook login loginmodalMsi-submit" value="Entra con Facebook "> -->
					     </a>
				    </form>
					
			  </div>
				</div>
			</div>
  
</div>
<!-- /Modal REGISTER -->


			<!--ENDA MODAL LOGIN CODE -->



				<?php
					the_content();
					wp_link_pages( $page_link_args );
				?>
				</section><!-- /.entry -->
			

			</article><!-- /.post -->

			<button class="homeParticipa_btn homeParticipa_orange" type="button" id="button_more_cards">
				<span><a href="#reply-title"><h1 style="color:white;">Colabora con el Proyecto</h1></a>
				</span>
			</button>

						
			<button class="homeParticipa_btn homeParticipa_orange" type="button">
				<span><h1 style="color:white;"><?php echo get_comments_number(); ?> Personas han Colaborado</h1>
				</span>
			</button>


			<?php comments_template(); ?>
		<?php } ?>
	<?php } ?>
	
	</section><!-- /#main -->

</div><!-- /#content -->

<?php get_footer(); ?>