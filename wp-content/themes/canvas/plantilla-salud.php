  
<?php
/*
Template Name: Presupuesto Salud
*/

?>

<?php get_header(); ?>
<!--STAR CUSTOM CODE-->

<h1>Presupuesto Salud</h1>

<section class="presupuestoSectionMain">
  
  <input class="presupuestotabInput" id="tab1" type="radio" name="tabs" checked>
  <label class="presupuestotabLabel" for="tab1">2015</label>
    
  <input class="presupuestotabInput" id="tab2" type="radio" name="tabs">
  <label class="presupuestotabLabel" for="tab2">2016</label>
    
  <input class="presupuestotabInput" id="tab3" type="radio" name="tabs">
  <label class="presupuestotabLabel" for="tab3">Comparar</label>
    
  <section class="presupuestoSection" id="content1">
    
    <iframe class='highcharts-iframe' src='//cloud.highcharts.com/embed/adapef' style='border: 0; width: 100%; height: 500px'></iframe>
  
  </section>
    
  <section class="presupuestoSection" id="content2">
 
    <iframe class='highcharts-iframe' src='//cloud.highcharts.com/embed/abosaz' style='border: 0; width: 100%; height: 500px'></iframe>

  </section>
    
  <section class="presupuestoSection" id="content3">
   
    <iframe class='highcharts-iframe' src='//cloud.highcharts.com/embed/iboxaq' style='border: 0; width: 100%; height: 500px'></iframe>

  </section>
    
    
</section>


<!--FINISH CUSTOM CODE-->

<?php get_footer(); ?>