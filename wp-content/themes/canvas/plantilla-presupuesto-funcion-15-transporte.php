  
<?php
/*
Template Name: Presupuesto Función Transporte
*/

?>

<?php get_header(); ?>
<!--STAR CUSTOM CODE-->

<h1>Transporte</h1>

<section class="presupuestoSectionMain">
  
  <input class="presupuestotabInput" id="tab1" type="radio" name="tabs" checked />
  <label class="presupuestotabLabel icon-2015" for="tab1">2015</label>
    
  <input class="presupuestotabInput" id="tab2" type="radio" name="tabs" />
  <label class="presupuestotabLabel icon-2015" for="tab2">2016</label>
    
  <input class="presupuestotabInput" id="tab3" type="radio" name="tabs" />
  <label class="presupuestotabLabel icon-compara" for="tab3">Comparar</label>
    
  <section class="presupuestoSection" id="content1">
    
    <!--iframe class='highcharts-iframe' src='//cloud.highcharts.com/embed/adapef' style='border: 0; width: 100%; height: 500px'></iframe-->
    <div id="area-transporte2015"></div>
  
  </section>
    
  <section class="presupuestoSection" id="content2">
 
    <!--iframe class='highcharts-iframe' src='//cloud.highcharts.com/embed/abosaz' style='border: 0; width: 100%; height: 500px'></iframe-->
    <div id="area-transporte2016"></div>

  </section>
    
  <section class="presupuestoSection" id="content3">
   
    <!--iframe class='highcharts-iframe' src='//cloud.highcharts.com/embed/iboxaq' style='border: 0; width: 100%; height: 500px'></iframe-->
    <div id="compara-transporte1516"></div>

  </section>

  <div class="presupuestoGerencias">
    <h4>Gerencias Involucradas en el Devengado de esta Función</h4>
    <p> 
        <ol>Ger. Desarrollo Distrital</ol>
        <ol>Ger. de Serguridad Ciudadana y Gestión de Riesgos de Desastres</ol>
    </p>
   </div>

    <div class="fuente-datos">
    <p>FUENTE: Estos datos han sido consultados desde el <a href="http://datosabiertos.msi.gob.pe/home">Portal de Datos Abieros de la Municipalidad de San Isidro</a></p>
  </div>
    
    
</section>


<!--FINISH CUSTOM CODE-->

<?php get_footer(); ?>