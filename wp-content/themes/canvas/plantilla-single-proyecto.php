<?php
/*
Template Name: Single Proyecto
*/
/// Set the slider
?>
<?php get_header(); ?>

<!--STAR CUSTOM CODE-->

<section class="singleProyecto-main">

	<section class="singleProyecto-img-container">
		
		<img class="singleProyecto-img" src="<?php echo site_url() ?>/wp-content/uploads/2016/03/coworking-img.jpg" alt="">

	</section>

<section class="singleProyecto-details">
	
	<div class="singleProyecto-details-1">
		<div class="singleProyecto-details-tittleProyect">
			<h2>Coworking en San Isidro</h2>
		</div>
		<div class="singleProyecto-details-cardDetails">
			Por: <a href="#">Perico los Palotes</a> 
			|<a href="#">Gerencia de Desarrollo Urbano</a> 
			|<a href="#">6 Colaboraciones</a> 
			|<a href="#">18 de febrero, 2016</a>
			|Num de Expediente: <a href="">MSI-5369</a>
		</div>
	</div>

	<div class="singleProyecto-details-2">
		
		<button class="homeParticipa_btn homeParticipa_orange singleProyecto-btnVote" type="button">
			<span>
				<h2 class="singleProyecto-h1-btn"style="color:white;">Vota</h2>
				<h2 class="singleProyecto-h1-btn"style="color:white;">18 votos</h2>
			</span>
		</button>

	</div>
</section>

	<!--STARTS COLABORACIÓN -->

	

	<article class="singleProyecto-postColaboracion">
		
		<p class="singleProyecto-postColaboracion-content">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			Nulla sed turpis arcu. Phasellus eu mattis arcu, ut rhoncus dolor. 
			Nunc aliquet, dui sit amet ullamcorper ultricies, quam sem finibus dui, 
			vitae elementum ipsum nibh vitae neque. Suspendisse accumsan malesuada lacinia.
			Aenean sit amet ante eget purus egestas feugiat a sed tortor. Vestibulum eleifend tellus 
			finibus dapibus elementum. Etiam vulputate gravida neque vitae ultricies. Sed vestibulum maximus 
			purus nec scelerisque. Fusce id scelerisque massa. Etiam tincidunt velit ipsum, id sodales
			magna condimentum et. Praesent eleifend vulputate gravida. Duis pellentesque eget nisi vel f
			acilisis. Nulla in nunc quam. Vivamus lobortis magna magna, vel commodo lorem consectetur vel.
			</br>
			</br>
			Curabitur eget enim eget augue eleifend cursus. Lorem ipsum dolor sit amet, consectetur adipiscing
			elit. Nulla id tincidunt nisi. Maecenas varius egestas augue at lobortis. Donec commodo sollicitudin 
			lacus sed sodales. Pellentesque interdum, lectus vel placerat fringilla, metus urna tincidunt ligula, 
			non elementum libero ante sed lectus. Nunc sodales lectus et erat blandit bibendum. Vestibulum viverra
			</br>
			</br>
			Luctus tellus at interdum. Vestibulum sed semper lorem. Duis tincidunt pharetra quam eget dictum.  
			https://www.youtube.com/watch?v=sVip_MpSoIU   Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
			Nulla sed turpis arcu. Phasellus eu mattis arcu, ut rhoncus dolor. Nunc aliquet, dui sit amet ullamcorper
			ultricies, quam sem finibus dui, vitae elementum ipsum nibh vitae neque. Suspendisse accumsan malesuad
			a lacinia. Aenean sit amet ante eget purus egestas feugiat a sed tortor. Vestibulum eleifend tellus finibus
			dapibus elementum. Etiam vulputate gravida neque vitae ultricies. Sed vestibulum maximus purus nec scelerisque.


		</p>

	</article>

	<!--ENDS COLABORACIÓN -->

	<section class="singleProyecto-stats">
		
		<div class="singleProyecto-btnVote">
			<button class="homeParticipa_btn homeParticipa_orange" type="button">
				<span><h1 style="color:white;">Colabora con la Propuesta</h1>
				</span>
			</button>
			
		</div>

		<div class="singleProyecto-numberColaboration">

			<button class="homeParticipa_btn homeParticipa_orange singleProyecto-btnTransparent" type="button">
				<span><h1 style="color:white;">06 Colaboraciones</h1>
				</span>
			</button>
			
		</div>


	</section>

	<section class="singleProyecto-colaboraciones"> <!--AQUI APARECEN LAS COLABORACIONES QUE SE HAN REALIZADO-->
		
		<h1>AQUI DEBERIA IR LOS COMENTARIO O LAS COLABORARIONES</h1>
	</section>

	<section class="singleProyecto-formColaboraciones"> <!--AQUI ES EL CAMPO PARA DEJAR EL COMENTARIO O COLABORACION PARA LA PROPUESTA-->
		<h1>AQUI DEBE ESTAR EL FIELD PARA PODER INGRESAR UN COMENTARIO O COLABORACION.</h1>

	</section>









</section>

<!--FINISH CUSTOM CODE-->

<?php get_footer(); ?>