	
<?php
/*
Template Name: Thanks You Page Project
*/

?>

<?php get_header(); ?>
<!--STAR CUSTOM CODE-->

<section>
		<section>
			<div class="thanksPage-h1"><h1>¡Gracias por tu Propuesta!</h1>
			</div>
		</section>
	<section class="thanksPage-divCheck">
		
		<div class="thanksPage-check icon-check" style="width: 100%;"></div>
		<p class="thanksPage-p">La hemos recibido y estara<br>siendo publicada dentro de poco.<br><br>Te notificaremos sobre su aprobación.</p>

	</section>
	
	<section>
		<button type="button" class="thanksPage-backbutton homeParticipa_btn homeParticipa_orange"><a class="thanksPage-backbutton-a" href="<?php echo site_url() ?>/participacion">
			<h2 class="thanksPage-backbutton-h2">Regresar a ver los Proyectos</h2></a></button>
	</section>


</section>

<!--FINISH CUSTOM CODE-->

<?php get_footer(); ?>