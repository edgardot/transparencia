	
<?php
/*
Template Name: Formulario Participativo
*/

?>

<?php get_header(); ?>
<!--STAR CUSTOM CODE-->

<section class="formGobAbierto-main">

<!-- <div class="title-formPropuesta">
	<h2>ENVÍA TU PROPUESTA</h2>
</div>

<div class="message-formPropuesta"> 
	<p>
		¡Tu idea es muy importante! y nos ayudará a mejorar la 
		calidad de vida y de servicios recibidos por los ciudadanos.
	</p>
</div> -->

<?php
	if(isset($_GET['msg'])) {

		switch($_GET['msg']) {
			case 'sucess' : echo '<div class="alert alert-success">Tu colaboración a subido con éxito, en breves momentos le notificaremos a su correo.</div>'; break;
			case 'error' : echo '<div class="alert alert-error">Hubo un error, tu colaboración no fue enviada.</div>'; break;
		}

	}
?>

<form class="formGobAbierto" method="POST" enctype="multipart/form-data">
  <h1 class="formGobAbierto-h1">Participa con tu propuesta</h1>
  	<p class="formGobAbierto-tittle-p">
		¡Tu idea es muy importante! y nos ayudará a mejorar la 
		calidad de vida y de servicios recibidos por los ciudadanos.
	</p>
  
  <input placeholder="Nombre de tu Propuesta" maxlength="45" type="text" value="" name="roboto_name" required>
  
  
					
	<label class="label-proyecto" for="file">Sube una imágen que acompañe  tu propuesta</label>
	<input class="uploadFile-roboto" type="file" id="file" name="roboto_img" placeholder="Sube una imágen que acompañe  tu propuesta" accept="image/*" required="required" />


	<!--START / SELECTOR DE CATEGORIAS-->
	  
	    
	  <div class="form-group">
	  		<?php $array_terms = get_terms('tema', array('hide_empty' => 0)); ?>
	    	<?php if ( ! empty( $array_terms ) && ! is_wp_error( $array_terms ) ) { ?>
						

      	<select class="formGobAbierto-select" name="roboto_category">
      		<?php foreach($array_terms as $term) { ?>
        	
        	<option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
        	<?php } ?>
        	
        </select>
        	<?php } ?>
    	  <label for="select" class="control-label">Categoria para la propuesta</label><i class="bar"></i>
       </div>
       
	    	       
	<!--END / SELECTOR DE CATEGORIAS-->  
	 
  <!-- <input placeholder="Email address" type="email" onblur="this.setAttribute('value', this.value);" value="" required>
  <span class="validation-text">Please enter a valid email address.</span>
  
  <input placeholder="Location" type="text" value="" required> -->
  
  <div class="flex">
    <textarea class="formGobabierto-textArea" placeholder="Participa aqui con una propuesta" name="roboto_desc" id="formProyecto" rows="10"  required></textarea>

	
  </div>
  
  	<div class="formGobabierto-button-container">
  		<button class="formGobAbierto-button homeParticipa_btn homeParticipa_orange" name="roboto_submit">Enviar Propuesta</button>
	</div>

</form>



<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Roboto:400' rel='stylesheet' type='text/css'>

</section>

<!--FINISH CUSTOM CODE-->
<?php get_footer(); ?>