jQuery(function ($) {

    // $(document).ready(function () {

        // Build the chart
    $('#anual2014').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Presupuesto del Año 2014 - Todas las Funciones'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Plan. Gest. y Reserv. de Contingencia',
                    y: 40.97
                }, {
                    name: 'Orden Público Y Segur.',
                    y: 16.49,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Comercio',
                    y: 4.44
                }, {
                    name: 'Transporte',
                    y: 8.06
                }, {
                    name: 'Ambiente',
                    y: 17.48
                }, {
                    name: 'Viv. Y Desarrollo Urbano',
                    y: 4.57
                }, {
                    name: 'Salud',
                    y: 0.78
                }, {
                    name: 'Cultura y Deporte',
                    y: 1.79
                }, {
                    name: 'Protección Social',
                    y: 1.92
                }, {
                    name: 'Prevision Social',
                    y: 3.50
                }]
            }]
        });


    // ANUAL 2015
        // Build the chart
       
        // Build the chart
$('#anual2015').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Presupuesto Ejecutado del Año 2015 - Todas las Funciones'
            },
            subtitle: {
                text: '<b>Total Ejecutado: S/.168,842,394.96</b>' 
            },
            tooltip: {
                pointFormat: '{series.name}: S/.<b>{point.y}</b>'
            },
            plotOptions: {
                  pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
            },
            series: [{
                name: 'Total Ejecutado',
                colorByPoint: true,
                data: [{
                    name: 'Plan. Gest. y <br>Reserv. de <br>Contingencia',
                    y: 62003128.04
                }, {
                    name: 'Orden Público Y Segur.',
                    y: 28694653.74,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Comercio',
                    y: 7062013.96
                }, {
                    name: 'Transporte',
                    y: 11649252.75
                }, {
                    name: 'Ambiente',
                    y: 33913506.23
                }, {
                    name: 'Viv. y Des. Urbano',
                    y: 8648913.85
                }, {
                    name: 'Salud',
                    y: 1111382.12
                }, {
                    name: 'Cultura y Deporte',
                    y: 5797501.23
                }, {
                    name: 'Protección Social',
                    y: 3911849.16
                }, {
                    name: 'Prevision Social',
                    y: 6050193.88
                }]
            }]
        });
    


   // ANUAL 2016

        // Build the chart
$('#anual2016').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Presupuesto Ejecutado del Año 2016 - Hasta Febrero'
            },
            subtitle: {
                text: '<b>Ejec: S/.210,812,997.00</b>' 
            },
            tooltip: {
                pointFormat: '{series.name}: <b>S/.{point.y}</b>'
            },
            plotOptions: {
                  pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
            },
            series: [{
                name: 'Presupuesto Ejecutado',
                colorByPoint: true,
                data: [{
                    name: 'Plan. Gest. y <br>Reserv. de <br>Contingencia',
                    y: 5972011.34
                }, {
                    name: 'Orden Público Y Segur.',
                    y: 3455015.14,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Comercio',
                    y: 1128724.8
                }, {
                    name: 'Transporte',
                    y: 1373678.87
                }, {
                    name: 'Ambiente',
                    y: 1624122.68
                }, {
                    name: 'Viv. y Des. Urbano',
                    y: 1128259.68
                }, {
                    name: 'Salud',
                    y: 194253.6
                }, {
                    name: 'Cultura y Deporte',
                    y: 1029545.41
                }, {
                    name: 'Protección Social',
                    y: 458879.62
                }, {
                    name: 'Prevision Social',
                    y: 832020.57
                }]
            }]
        });


// MENSUAL
$('#mensual2014').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Preusupuesto usado durante el 2014'
        },
        subtitle: {
            text: 'Clic para ver el detalle de cada categoria durante los meses que corresponden'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: false,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>S/.{point.y}</b><br/>'
        },

        series: [{
            name: 'Meses',
            colorByPoint: true,
            data: [{
                name: 'Ene',
                y: 17194945.73,
                drilldown: 'Ene'
            }, {
                name: 'Feb',
                y: 12085771.60,
                drilldown: 'Feb'
            }, {
                name: 'Mar',
                y: 17142542.33,
                drilldown: 'Mar'
            }, {
                name: 'Abr',
                y: 18587406.59  ,
                drilldown: 'Abr'
            }, {
                name: 'May',
                y: 15013025.49,
                drilldown: 'May'
            }, {
                name: 'Jun',
                y: 10762024.64,
                drilldown: 'Jun'
            },{
                name: 'Jul',
                y: 17833780.50,
                drilldown: 'Jul'
            },{
                name: 'Agosto',
                y: 11539127.67,
                drilldown: 'Agosto'
            },{
                name: 'Sep',
                y: 11717591.99,
                drilldown: 'Sep'
            },{
                name: 'Oct',
                y: 16230330.87,
                drilldown: 'Oct'
            },{
                name: 'Nov',
                y: 10589057.80,
                drilldown: 'Nov'
            },{
                name: 'Dic',
                y: 19850113.54,
                drilldown: 'Dic'
            }]
        }],
        drilldown: {
            series: [{
                name: 'Ene',
                id: 'Ene',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        12411933.61

                    ],
                    [
                        'Orden Pub Y Seg',
                        1522281.62

                    ],
                    [
                        'Comercio',
                        631672.65

                    ],
                    [
                        'Transporte',
                        281008.45

                    ],
                    [
                        'Ambiente',
                        738551.74

                    ],
                    [
                        'Viv y Des Urbano',
                        695003.92

                    ],
                    [
                        'Salud',
                        91248.73

                    ],
                    [
                        'Cultura Y Deporte',
                        168438.42

                    ],
                    [
                        'Protección Social',
                        223574.04

                    ],
                    [
                        'Prevision Social',
                        431232.55

                    ]
                ]
            },{
                name: 'Feb',
                id: 'Feb',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4360848.24

                    ],
                    [
                        'Orden Pub Y Seg',
                        1918010.31

                    ],
                    [
                        'Comercio',
                        539842.32

                    ],
                    [
                        'Transporte',
                        931856.11

                    ],
                    [
                        'Ambiente',
                        2689881.56

                    ],
                    [
                        'Viv y Des Urbano',
                        678196.59

                    ],
                    [
                        'Salud',
                        73003.8

                    ],
                    [
                        'Cultura Y Deporte',
                        210906.87

                    ],
                    [
                        'Protección Social',
                        231743.42

                    ],
                    [
                        'Prevision Social',
                        451482.38

                    ]
                ]
            },{
                name: 'Mar',
                id: 'Mar',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        6649862.32

                    ],
                    [
                        'Orden Pub Y Seg',
                        1822305.38

                    ],
                    [
                        'Comercio',
                        563239.61

                    ],
                    [
                        'Transporte',
                        2140402.53

                    ],
                    [
                        'Ambiente',
                        4049999.78

                    ],
                    [
                        'Viv y Des Urbano',
                        875808.91

                    ],
                    [
                        'Salud',
                        79289.09

                    ],
                    [
                        'Cultura Y Deporte',
                        235833.36

                    ],
                    [
                        'Protección Social',
                        254988.11

                    ],
                    [
                        'Prevision Social',
                        470813.24

                    ]
                ]
            }, {
                name: 'Abr',
                id: 'Abr',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        6672969.69

                    ],
                    [
                        'Orden Pub Y Seg',
                        2311388.24

                    ],
                    [
                        'Comercio',
                        630581.67

                    ],
                    [
                        'Transporte',
                        2950340.28

                    ],
                    [
                        'Ambiente',
                        4243051.16

                    ],
                    [
                        'Viv y Des Urbano',
                        652283.77

                    ],
                    [
                        'Salud',
                        103472.16

                    ],
                    [
                        'Cultura Y Deporte',
                        276711.97

                    ],
                    [
                        'Protección Social',
                        279241.88

                    ],
                    [
                        'Prevision Social',
                        467365.77

                    ]
                ]
            }, {
                name: 'May',
                id: 'May',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        7617097.41

                    ],
                    [
                        'Orden Pub Y Seg',
                        1881480.82

                    ],
                    [
                        'Comercio',
                        628844.63

                    ],
                    [
                        'Transporte',
                        851922.32

                    ],
                    [
                        'Ambiente',
                        2302840.94

                    ],
                    [
                        'Viv y Des Urbano',
                        688761.96

                    ],
                    [
                        'Salud',
                        99566.73

                    ],
                    [
                        'Cultura Y Deporte',
                        218797.11

                    ],
                    [
                        'Protección Social',
                        237034.62

                    ],
                    [
                        'Prevision Social',
                        486678.95

                    ]
                ]
            },{
                name: 'Jun',
                id: 'Jun',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4498738.05

                    ],
                    [
                        'Orden Pub Y Seg',
                        2276276.05

                    ],
                    [
                        'Comercio',
                        632540.12

                    ],
                    [
                        'Transporte',
                        1013399.27

                    ],
                    [
                        'Ambiente',
                        655888.51


                    ],
                    [
                        'Viv y Des Urbano',
                        593035.19

                    ],
                    [
                        'Salud',
                        108718.52

                    ],
                    [
                        'Cultura Y Deporte',
                        281997.76

                    ],
                    [
                        'Protección Social',
                        275222.32

                    ],
                    [
                        'Prevision Social',
                        426208.85

                    ]
                ]
            },{
                name: 'Jul',
                id: 'Jul',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        6023379.91

                    ],
                    [
                        'Orden Pub Y Seg',
                        3499947.28

                    ],
                    [
                        'Comercio',
                        824460.41

                    ],
                    [
                        'Transporte',
                        661470.72

                    ],
                    [
                        'Ambiente',
                        4326497.68

                    ],
                    [
                        'Viv y Des Urbano',
                        743583.67

                    ],
                    [
                        'Salud',
                        161403.16

                    ],
                    [
                        'Cultura Y Deporte',
                        389995.36

                    ],
                    [
                        'Protección Social',
                        324950.75

                    ],
                    [
                        'Prevision Social',
                        878091.56

                    ]
                ]
            },{
                name: 'Ago',
                id: 'Ago',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4564336.25

                    ],
                    [
                        'Orden Pub Y Seg',
                        2947367.72

                    ],
                    [
                        'Comercio',
                        636784.88


                    ],
                    [
                        'Transporte',
                        828803.63


                    ],
                    [
                        'Ambiente',
                        899018.33

                    ],
                    [
                        'Viv y Des Urbano',
                        605170.58

                    ],
                    [
                        'Salud',
                        106300.95

                    ],
                    [
                        'Cultura Y Deporte',
                        214027.63

                    ],
                    [
                        'Protección Social',
                        285565.61

                    ],
                    [
                        'Prevision Social',
                        451752.09

                    ]
                ]
            },{
                name: 'Sep',
                id: 'Sep',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4076635.95

                    ],
                    [
                        'Orden Pub Y Seg',
                        2303797.58

                    ],
                    [
                        'Comercio',
                        576829.94

                    ],
                    [
                        'Transporte',
                        1006298.20

                    ],
                    [
                        'Ambiente',
                        2188200.45

                    ],
                    [
                        'Viv y Des Urbano',
                        553914.89

                    ],
                    [
                        'Salud',
                        125615.47

                    ],
                    [
                        'Cultura Y Deporte',
                        211559.44

                    ],
                    [
                        'Protección Social',
                        221264.43

                    ],
                    [
                        'Prevision Social',
                        453475.64

                    ]
                ]
            },{
                name: 'Oct',
                id: 'Oct',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        5763359.62

                    ],
                    [
                        'Orden Pub Y Seg',
                        2519174.99

                    ],
                    [
                        'Comercio',
                        737000.94

                    ],
                    [
                        'Transporte',
                        1354572.69

                    ],
                    [
                        'Ambiente',
                        4041728.78

                    ],
                    [
                        'Viv y Des Urbano',
                        616563.93

                    ],
                    [
                        'Salud',
                        170495.71

                    ],
                    [
                        'Cultura Y Deporte',
                        254919.42

                    ],
                    [
                        'Protección Social',
                        328176.01

                    ],
                    [
                        'Prevision Social',
                        444338.78

                    ]
                ]
            },{
                name: 'Nov',
                id: 'Nov',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4363603.41

                    ],
                    [
                        'Orden Pub Y Seg',
                        2546905.54

                    ],
                    [
                        'Comercio',
                        612110.13

                    ],
                    [
                        'Transporte',
                        767141.20


                    ],
                    [
                        'Ambiente',
                        479435.38

                    ],
                    [
                        'Viv y Des Urbano',
                        621835.03

                    ],
                    [
                        'Salud',
                        113710.31

                    ],
                    [
                        'Cultura Y Deporte',
                        333669.53

                    ],
                    [
                        'Protección Social',
                        310039.42

                    ],
                    [
                        'Prevision Social',
                        440607.85

                    ]
                ]
            },{
                name: 'Dic',
                id: 'Dic',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        6145373.90

                    ],
                    [
                        'Orden Pub Y Seg',
                        3898987.38

                    ],
                    [
                        'Comercio',
                        912155.75

                    ],
                    [
                        'Transporte',
                        1611885.29

                    ],
                    [
                        'Ambiente',
                        4590365.76

                    ],
                    [
                        'Viv y Des Urbano',
                        842802.34


                    ],
                    [
                        'Salud',
                        153672.26

                    ],
                    [
                        'Cultura Y Deporte',
                        392035.82


                    ],
                    [
                        'Protección Social',
                        459023.53


                    ],
                    [
                        'Prevision Social',
                        843811.51

                    ]
                  ]
                }]
            } 
        });

//MENSUAL 2015
    // Create the chart
$('#mensual2015').highcharts({
         chart: {
            type: 'column'
        },
        title: {
            text: 'Preusupuesto Ejecutado durante el 2015<br><b>Total Ejecutado: S/.168,842,394.96</b>'
        },
        subtitle: {
            text: 'Clic para ver el detalle de cada categoria durante los meses que corresponden'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: false,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>S/.{point.y}</b><br/>'
        },

        series: [{
            name: 'Meses',
            colorByPoint: true,
            data: [{
                name: 'Ene',
                y: 7610032.39,
                drilldown: 'Ene'
            }, {
                name: 'Feb',
                y: 9612610.25,
                drilldown: 'Feb'
            }, {
                name: 'Mar',
                y: 11483245.34,
                drilldown: 'Mar'
            }, {
                name: 'Abr',
                y: 11595279.61,
                drilldown: 'Abr'
            }, {
                name: 'May',
                y: 11595279.61,
                drilldown: 'May'
            }, {
                name: 'Jun',
                y: 9228540.00,
                drilldown: 'Jun'
            },{
                name: 'Jul',
                y: 15893909.98,
                drilldown: 'Jul'
            },{
                name: 'Agosto',
                y: 12767141.09,
                drilldown: 'Agosto'
            },{
                name: 'Sep',
                y: 23441357.08,
                drilldown: 'Sep'
            },{
                name: 'Oct',
                y: 10832276.60,
                drilldown: 'Oct'
            },{
                name: 'Nov',
                y: 14505440.03,
                drilldown: 'Nov'
            },{
                name: 'Dic',
                y: 30110699.51,
                drilldown: 'Dic'
            }]
        }],
        drilldown: {
            series: [{
                name: 'Ene',
                id: 'Ene',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        2295611.19

                    ],
                    [
                        'Orden Pub Y Seg',
                        1388795.17

                    ],
                    [
                        'Comercio',
                        628682.93

                    ],
                    [
                        'Transporte',
                        393313.67

                    ],
                    [
                        'Ambiente',
                        1227013.09

                    ],
                    [
                        'Viv y Des Urbano',
                        836333.22

                    ],
                    [
                        'Salud',
                        83066.83

                    ],
                    [
                        'Cultura Y Deporte',
                        151991.08

                    ],
                    [
                        'Protección Social',
                        184030.14

                    ],
                    [
                        'Prevision Social',
                        421195.07

                    ]
                ]
            },{
                name: 'Feb',
                id: 'Feb',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        2862788.65

                    ],
                    [
                        'Orden Pub Y Seg',
                        1917165.87

                    ],
                    [
                        'Comercio',
                        493847.27

                    ],
                    [
                        'Transporte',
                        595560.22

                    ],
                    [
                        'Ambiente',
                        2407389.50

                    ],
                    [
                        'Viv y Des Urbano',
                        516172.00

                    ],
                    [
                        'Salud',
                        56475.58

                    ],
                    [
                        'Cultura Y Deporte',
                        133103.44

                    ],
                    [
                        'Protección Social',
                        185551.05

                    ],
                    [
                        'Prevision Social',
                        444556.67

                    ]
                ]
            },{
                name: 'Mar',
                id: 'Mar',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3677816.84

                    ],
                    [
                        'Orden Pub Y Seg',
                        1962108.20

                    ],
                    [
                        'Comercio',
                        588113.13

                    ],
                    [
                        'Transporte',
                        642560.01

                    ],
                    [
                        'Ambiente',
                        2862421.54

                    ],
                    [
                        'Viv y Des Urbano',
                        757811.76

                    ],
                    [
                        'Salud',
                        75779.30

                    ],
                    [
                        'Cultura Y Deporte',
                        209179.34

                    ],
                    [
                        'Protección Social',
                        273505.96

                    ],
                    [
                        'Prevision Social',
                        433949.26

                    ]
                ]
            }, {
                name: 'Abr',
                id: 'Abr',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3600623.16

                    ],
                    [
                        'Orden Pub Y Seg',
                        1914619.57

                    ],
                    [
                        'Comercio',
                        552807.96

                    ],
                    [
                        'Transporte',
                        846832.57

                    ],
                    [
                        'Ambiente',
                        2673155.86

                    ],
                    [
                        'Viv y Des Urbano',
                        1034610.33

                    ],
                    [
                        'Salud',
                        64239.11

                    ],
                    [
                        'Cultura Y Deporte',
                        394454.83

                    ],
                    [
                        'Protección Social',
                        248999.10

                    ],
                    [
                        'Prevision Social',
                        431520.59

                    ]
                ]
            }, {
                name: 'May',
                id: 'May',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3615867.11

                    ],
                    [
                        'Orden Pub Y Seg',
                        2086334.70

                    ],
                    [
                        'Comercio',
                        570349.63

                    ],
                    [
                        'Transporte',
                        875961.67

                    ],
                    [
                        'Ambiente',
                        2646953.68

                    ],
                    [
                        'Viv y Des Urbano',
                        485828.78

                    ],
                    [
                        'Salud',
                        75620.05

                    ],
                    [
                        'Cultura Y Deporte',
                        440512.73

                    ],
                    [
                        'Protección Social',
                        291840.23

                    ],
                    [
                        'Prevision Social',
                        506011.03

                    ]
                ]
            },{
                name: 'Jun',
                id: 'Jun',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3749899.21

                    ],
                    [
                        'Orden Pub Y Seg',
                        1982075.32

                    ],
                    [
                        'Comercio',
                        482334.00

                    ],
                    [
                        'Transporte',
                        460453.43

                    ],
                    [
                        'Ambiente',
                        898755.48


                    ],
                    [
                        'Viv y Des Urbano',
                        568964.34

                    ],
                    [
                        'Salud',
                        79906.94

                    ],
                    [
                        'Cultura Y Deporte',
                        276593.78

                    ],
                    [
                        'Protección Social',
                        290408.70

                    ],
                    [
                        'Prevision Social',
                        439148.80

                    ]
                ]
            },{
                name: 'Jul',
                id: 'Jul',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4168769.49

                    ],
                    [
                        'Orden Pub Y Seg',
                        2493834.93

                    ],
                    [
                        'Comercio',
                        664308.33

                    ],
                    [
                        'Transporte',
                        1058736.73

                    ],
                    [
                        'Ambiente',
                        4839412.24

                    ],
                    [
                        'Viv y Des Urbano',
                        748139.71

                    ],
                    [
                        'Salud',
                        100990.22

                    ],
                    [
                        'Cultura Y Deporte',
                        591134.63

                    ],
                    [
                        'Protección Social',
                        377330.63

                    ],
                    [
                        'Prevision Social',
                        851253.07

                    ]
                ]
            },{
                name: 'Ago',
                id: 'Ago',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3459573.22

                    ],
                    [
                        'Orden Pub Y Seg',
                        2499135.80

                    ],
                    [
                        'Comercio',
                        489266.78


                    ],
                    [
                        'Transporte',
                        1286847.69


                    ],
                    [
                        'Ambiente',
                        3097830.48

                    ],
                    [
                        'Viv y Des Urbano',
                        808857.09

                    ],
                    [
                        'Salud',
                        92818.89

                    ],
                    [
                        'Cultura Y Deporte',
                        360600.35

                    ],
                    [
                        'Protección Social',
                        258921.45

                    ],
                    [
                        'Prevision Social',
                        413289.34

                    ]
                ]
            },{
                name: 'Sep',
                id: 'Sep',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        14779999.42

                    ],
                    [
                        'Orden Pub Y Seg',
                        2726243.04

                    ],
                    [
                        'Comercio',
                        511725.49

                    ],
                    [
                        'Transporte',
                        988839.74

                    ],
                    [
                        'Ambiente',
                        2711060.95

                    ],
                    [
                        'Viv y Des Urbano',
                        610475.89

                    ],
                    [
                        'Salud',
                        104280.65

                    ],
                    [
                        'Cultura Y Deporte',
                        300648.96

                    ],
                    [
                        'Protección Social',
                        295034.98

                    ],
                    [
                        'Prevision Social',
                        413047.96

                    ]
                ]
            },{
                name: 'Oct',
                id: 'Oct',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3434804.58

                    ],
                    [
                        'Orden Pub Y Seg',
                        2098918.30

                    ],
                    [
                        'Comercio',
                        532054.30

                    ],
                    [
                        'Transporte',
                        641308.28

                    ],
                    [
                        'Ambiente',
                        2385530.86

                    ],
                    [
                        'Viv y Des Urbano',
                        574100.84

                    ],
                    [
                        'Salud',
                        87437.37

                    ],
                    [
                        'Cultura Y Deporte',
                        326473.37

                    ],
                    [
                        'Protección Social',
                        315278.66

                    ],
                    [
                        'Prevision Social',
                        436370.04

                    ]
                ]
            },{
                name: 'Nov',
                id: 'Nov',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4008598.90

                    ],
                    [
                        'Orden Pub Y Seg',
                        3363269.32

                    ],
                    [
                        'Comercio',
                        676193.78

                    ],
                    [
                        'Transporte',
                        1261751.35


                    ],
                    [
                        'Ambiente',
                        2705010.35

                    ],
                    [
                        'Viv y Des Urbano',
                        684174.79

                    ],
                    [
                        'Salud',
                        101552.46

                    ],
                    [
                        'Cultura Y Deporte',
                        958199.18

                    ],
                    [
                        'Protección Social',
                        345706.04

                    ],
                    [
                        'Prevision Social',
                        400983.86

                    ]
                ]
            },{
                name: 'Dic',
                id: 'Dic',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        12348776.27

                    ],
                    [
                        'Orden Pub Y Seg',
                        4262153.52

                    ],
                    [
                        'Comercio',
                        872330.36

                    ],
                    [
                        'Transporte',
                        2597087.39

                    ],
                    [
                        'Ambiente',
                        5458972.20

                    ],
                    [
                        'Viv y Des Urbano',
                        1023445.10


                    ],
                    [
                        'Salud',
                        189214.72

                    ],
                    [
                        'Cultura Y Deporte',
                        1654609.54


                    ],
                    [
                        'Protección Social',
                        845242.22


                    ],
                    [
                        'Prevision Social',
                        858868.19

                    ]
                  ]
                }]
            }
    });



$('#mensual2016').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Preusupuesto Ejecutado durante el 2016<br><b>Ejecutado a la fecha: S/.17,196,511.71</b>'
        },
        subtitle: {
            text: 'Clic para ver el detalle de cada categoria durante los meses que corresponden'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: false,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>S/.{point.y}</b><br/>'
        },

        series: [{
            name: 'Meses',
            colorByPoint: true,
            data: [{
                name: 'Ene',
                y: 7558149.58,
                drilldown: 'Ene'
            }, {
                name: 'Feb',
                y: 9638362.13,
                drilldown: 'Feb'
            }, {
                name: 'Mar',
                y: 0,
                drilldown: 'Mar'
            }, {
                name: 'Abr',
                y: 0,
                drilldown: 'Abr'
            }, {
                name: 'May',
                y: 0,
                drilldown: 'May'
            }, {
                name: 'Jun',
                y: 0,
                drilldown: 'Jun'
            },{
                name: 'Jul',
                y: 0,
                drilldown: 'Jul'
            },{
                name: 'Agosto',
                y: 0,
                drilldown: 'Agosto'
            },{
                name: 'Sep',
                y: 0,
                drilldown: 'Sep'
            },{
                name: 'Oct',
                y: 0,
                drilldown: 'Oct'
            },{
                name: 'Nov',
                y: 0,
                drilldown: 'Nov'
            },{
                name: 'Dic',
                y: 0,
                drilldown: 'Dic'
            }]
        }],
        drilldown: {
            series: [{
                name: 'Ene',
                id: 'Ene',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        2488641.17

                    ],
                    [
                        'Orden Pub Y Seg',
                        1584043.16

                    ],
                    [
                        'Comercio',
                        607695.06

                    ],
                    [
                        'Transporte',
                        578745.31

                    ],
                    [
                        'Ambiente',
                        720473.57

                    ],
                    [
                        'Viv y Des Urbano',
                        570596.7

                    ],
                    [
                        'Salud',
                        102966.3

                    ],
                    [
                        'Cultura Y Deporte',
                        274709.12

                    ],
                    [
                        'Protección Social',
                        228309.2

                    ],
                    [
                        'Prevision Social',
                        401969.99


                    ]
                ]
            },{
                name: 'Feb',
                id: 'Feb',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3483370.17

                    ],
                    [
                        'Orden Pub Y Seg',
                        1870971.98

                    ],
                    [
                        'Comercio',
                        521029.74

                    ],
                    [
                        'Transporte',
                        794933.56

                    ],
                    [
                        'Ambiente',
                        903649.11

                    ],
                    [
                        'Viv y Des Urbano',
                        557662.98

                    ],
                    [
                        'Salud',
                        91287.3

                    ],
                    [
                        'Cultura Y Deporte',
                        754836.29

                    ],
                    [
                        'Protección Social',
                        230570.42

                    ],
                    [
                        'Prevision Social',
                        430050.58

                    ]
                ]
            },{
                name: 'Mar',
                id: 'Mar',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        0

                    ],
                    [
                        'Orden Pub Y Seg',
                        0

                    ],
                    [
                        'Comercio',
                        0

                    ],
                    [
                        'Transporte',
                        0

                    ],
                    [
                        'Ambiente',
                        0

                    ],
                    [
                        'Viv y Des Urbano',
                        0

                    ],
                    [
                        'Salud',
                        0

                    ],
                    [
                        'Cultura Y Deporte',
                        209179.34

                    ],
                    [
                        'Protección Social',
                        0

                    ],
                    [
                        'Prevision Social',
                        0

                    ]
                ]
            }, {
                name: 'Abr',
                id: 'Abr',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3600623.16

                    ],
                    [
                        'Orden Pub Y Seg',
                        1914619.57

                    ],
                    [
                        'Comercio',
                        552807.96

                    ],
                    [
                        'Transporte',
                        846832.57

                    ],
                    [
                        'Ambiente',
                        2673155.86

                    ],
                    [
                        'Viv y Des Urbano',
                        1034610.33

                    ],
                    [
                        'Salud',
                        64239.11

                    ],
                    [
                        'Cultura Y Deporte',
                        394454.83

                    ],
                    [
                        'Protección Social',
                        248999.10

                    ],
                    [
                        'Prevision Social',
                        431520.59

                    ]
                ]
            }, {
                name: 'May',
                id: 'May',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3615867.11

                    ],
                    [
                        'Orden Pub Y Seg',
                        2086334.70

                    ],
                    [
                        'Comercio',
                        570349.63

                    ],
                    [
                        'Transporte',
                        875961.67

                    ],
                    [
                        'Ambiente',
                        2646953.68

                    ],
                    [
                        'Viv y Des Urbano',
                        485828.78

                    ],
                    [
                        'Salud',
                        75620.05

                    ],
                    [
                        'Cultura Y Deporte',
                        440512.73

                    ],
                    [
                        'Protección Social',
                        291840.23

                    ],
                    [
                        'Prevision Social',
                        506011.03

                    ]
                ]
            },{
                name: 'Jun',
                id: 'Jun',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3749899.21

                    ],
                    [
                        'Orden Pub Y Seg',
                        1982075.32

                    ],
                    [
                        'Comercio',
                        482334.00

                    ],
                    [
                        'Transporte',
                        460453.43

                    ],
                    [
                        'Ambiente',
                        898755.48


                    ],
                    [
                        'Viv y Des Urbano',
                        568964.34

                    ],
                    [
                        'Salud',
                        79906.94

                    ],
                    [
                        'Cultura Y Deporte',
                        276593.78

                    ],
                    [
                        'Protección Social',
                        290408.70

                    ],
                    [
                        'Prevision Social',
                        439148.80

                    ]
                ]
            },{
                name: 'Jul',
                id: 'Jul',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4168769.49

                    ],
                    [
                        'Orden Pub Y Seg',
                        2493834.93

                    ],
                    [
                        'Comercio',
                        664308.33

                    ],
                    [
                        'Transporte',
                        1058736.73

                    ],
                    [
                        'Ambiente',
                        4839412.24

                    ],
                    [
                        'Viv y Des Urbano',
                        748139.71

                    ],
                    [
                        'Salud',
                        100990.22

                    ],
                    [
                        'Cultura Y Deporte',
                        591134.63

                    ],
                    [
                        'Protección Social',
                        377330.63

                    ],
                    [
                        'Prevision Social',
                        851253.07

                    ]
                ]
            },{
                name: 'Ago',
                id: 'Ago',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3459573.22

                    ],
                    [
                        'Orden Pub Y Seg',
                        2499135.80

                    ],
                    [
                        'Comercio',
                        489266.78


                    ],
                    [
                        'Transporte',
                        1286847.69


                    ],
                    [
                        'Ambiente',
                        3097830.48

                    ],
                    [
                        'Viv y Des Urbano',
                        808857.09

                    ],
                    [
                        'Salud',
                        92818.89

                    ],
                    [
                        'Cultura Y Deporte',
                        360600.35

                    ],
                    [
                        'Protección Social',
                        258921.45

                    ],
                    [
                        'Prevision Social',
                        413289.34

                    ]
                ]
            },{
                name: 'Sep',
                id: 'Sep',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        14779999.42

                    ],
                    [
                        'Orden Pub Y Seg',
                        2726243.04

                    ],
                    [
                        'Comercio',
                        511725.49

                    ],
                    [
                        'Transporte',
                        988839.74

                    ],
                    [
                        'Ambiente',
                        2711060.95

                    ],
                    [
                        'Viv y Des Urbano',
                        610475.89

                    ],
                    [
                        'Salud',
                        104280.65

                    ],
                    [
                        'Cultura Y Deporte',
                        300648.96

                    ],
                    [
                        'Protección Social',
                        295034.98

                    ],
                    [
                        'Prevision Social',
                        413047.96

                    ]
                ]
            },{
                name: 'Oct',
                id: 'Oct',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        3434804.58

                    ],
                    [
                        'Orden Pub Y Seg',
                        2098918.30

                    ],
                    [
                        'Comercio',
                        532054.30

                    ],
                    [
                        'Transporte',
                        641308.28

                    ],
                    [
                        'Ambiente',
                        2385530.86

                    ],
                    [
                        'Viv y Des Urbano',
                        574100.84

                    ],
                    [
                        'Salud',
                        87437.37

                    ],
                    [
                        'Cultura Y Deporte',
                        326473.37

                    ],
                    [
                        'Protección Social',
                        315278.66

                    ],
                    [
                        'Prevision Social',
                        436370.04

                    ]
                ]
            },{
                name: 'Nov',
                id: 'Nov',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        4008598.90

                    ],
                    [
                        'Orden Pub Y Seg',
                        3363269.32

                    ],
                    [
                        'Comercio',
                        676193.78

                    ],
                    [
                        'Transporte',
                        1261751.35


                    ],
                    [
                        'Ambiente',
                        2705010.35

                    ],
                    [
                        'Viv y Des Urbano',
                        684174.79

                    ],
                    [
                        'Salud',
                        101552.46

                    ],
                    [
                        'Cultura Y Deporte',
                        958199.18

                    ],
                    [
                        'Protección Social',
                        345706.04

                    ],
                    [
                        'Prevision Social',
                        400983.86

                    ]
                ]
            },{
                name: 'Dic',
                id: 'Dic',
                data: [
                    [
                        'Plan, Gest y Res de Contingencia',
                        12348776.27

                    ],
                    [
                        'Orden Pub Y Seg',
                        4262153.52

                    ],
                    [
                        'Comercio',
                        872330.36

                    ],
                    [
                        'Transporte',
                        2597087.39

                    ],
                    [
                        'Ambiente',
                        5458972.20

                    ],
                    [
                        'Viv y Des Urbano',
                        1023445.10


                    ],
                    [
                        'Salud',
                        189214.72

                    ],
                    [
                        'Cultura Y Deporte',
                        1654609.54


                    ],
                    [
                        'Protección Social',
                        845242.22


                    ],
                    [
                        'Prevision Social',
                        858868.19

                    ]
                  ]
                }]
              }
        
    });


$('#comparar_2015_2016').highcharts({
        chart: {
            zoomType: 'xy',
            type: 'column'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 7610032.39,
                    9612610.25,
                    11483245.34,
                    11761863.08,
                    11595279.61,
                    9228540.00,
                    15893909.98,
                    12767141.09,
                    23441357.08,
                    10832276.6,
                    14505440.03,
                    30110699.51
                    ]
        }, {
            name: '2016',
            data: [ 7558149.58,
                    9638362.13,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                    ]
        }]
    });


//cat
    $('#cat1').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Plan. Gest y Prev de Contingencia - 2014'
        },
        subtitle: {
            text: 'Source: <a href="http://thebulletin.metapress.com/content/c4120650912x74k7/fulltext.pdf">' +
                'thebulletin.metapress.com</a>'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b>{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: '2014 - Presupuesto Usado',
            data: [ 12411933.61,
                    4360848.24,
                    6649862.32,
                    6672969.69,
                    7617097.41,
                    4498738.05,
                    6023379.91,
                    4564336.25,
                    4076635.95,
                    5763359.62,
                    4363603.41,
                    6145373.9]
        }]
    });


    $('#comp1415').highcharts({
        title: {
            text: 'Comparación entre los años 2014 y 2015',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Usado y Devengado',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2014',
            data: [ 17194945.73,    
                    12085771.60,
                    17142542.33,
                    18587406.59,    
                    15013025.49,
                    10762024.64,    
                    17833780.50,    
                    11539127.67,    
                    11717591.99,    
                    16230330.87,    
                    10589057.80,    
                    19850113.54]
        }, {
            name: '2015',
            data: [ 7610032.39, 
                    9612610.25, 
                    11483245.34,    
                    11761863.08,
                    11595279.61,    
                    9228540.00,
                    15893909.98,    
                    12767141.09,
                    23441357.08,    
                    10832276.60,
                    14505440.03,    
                    30110699.51]
        }]
    });

    
    $('#funciones2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Funciones Año 2014'
        },
        xAxis: {
                max: 9,
            categories: [                       
            'Plan, Gest y Res de Cont',
                        'Orden Púb y Seg',
                        'Comercio',
                        'Transporte',
                        'Ambiente',
                        'Viv y Des Urbano',
                        'Salud',
                        'Cultura Y Deporte',
                        'Protección Social',
                        'Prevision Social']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Categorias por Anio'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>S/.{point.y}</b><br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 73148138.36,
                    29447922.91,
                    7926063.05,
                    14399100.69,
                    31205460.07,
                    8166960.78,
                    1386496.89,
                    3188892.69,
                    3430824.14,
                    6245859.17]
        }]
    });

$('#plan2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Devengado Mensua Año 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
                        'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Planeamiento Gestión y Reserva - Mensual / Año 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 12411933.61,
                    4360848.24,
                    6649862.32,
                    6672969.69,
                    7617097.41,
                    4498738.05,
                    6023379.91,
                    4564336.25,
                    4076635.95,
                    5763359.62,
                    4363603.41,
                    6145373.9]
        }]
    });

$('#plan2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Planeamiento Gestión y Reserva - Mensual / Año 2015'
        },
        subtitle: {
            text: 'Total Ejecutado: S/.62,003,128.04'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Presupuesto expresado en Millones de Soles'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 2295611.19, 
                    2862788.65, 
                    3677816.84, 
                    3600623.16, 
                    3615867.11, 
                    3749899.21, 
                    4168769.49, 
                    3459573.22, 
                    14779999.42,    
                    3434804.58,
                    4008598.90, 
                    12348776.27]
        }]
    });

$('#seguridad2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Orden Público y Seguridad - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 1522281.62,
                    1918010.31,
                    1822305.38,
                    2311388.24,
                    1881480.82,
                    2276276.05,
                    3499947.28,
                    2947367.72,
                    2303797.58,
                    2519174.99,
                    2546905.54,
                    3898987.38 ]
         }]
    });

$('#plan2016').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Planeamiento Gestión y Reserva Mensual / Año 2016'
        },
        xAxis: {
                max: 11,
            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado de todas las categorias'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [2488641.17,
                  1584043.16,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00
                  ]
        }]
    }); 


 $('#seguridad2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Orden Público y Seguridad - 2015'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2015'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 1388795.17, 
                    1917165.87, 
                    1962108.20, 
                    1914619.57, 
                    2086334.70, 
                    1982075.32, 
                    2493834.93, 
                    2499135.80,
                    2726243.04, 
                    2098918.30, 
                    3363269.32, 
                    4262153.52]
         }]
    });

$('#comercio2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Comercio - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 631672.65,
                    539842.32,
                    563239.61,
                    630581.67,
                    628844.63,
                    632540.12,
                    824460.41,
                    636784.88,
                    576829.94,
                    737000.94,
                    612110.13,
                    912155.75 ]
         }]
    });

$('#comercio2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Comercio - 2015'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2015'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 628682.93,  
                    493847.27,  
                    588113.13,  
                    552807.96,  
                    570349.63,  
                    482334, 
                    664308.33,  
                    489266.78,  
                    511725.49,  
                    532054.30,  
                    676193.78,  
                    872330.36 ]
         }]
    });

 $('#transporte2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '15 - Transporte - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 281008.45,
                    931856.11,
                    2140402.53,
                    2950340.28,
                    851922.32,
                    1013399.27,
                    661470.72,
                    828803.63,
                    1006298.20,
                    1354572.69,
                    767141.20,
                    1611885.29 ]
         }]
    });

 $('#transporte2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '15 - Transporte - 2015'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2015'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 393313.67,  
                    595560.22,  
                    642560.01,  
                    846832.57,  
                    875961.67,  
                    460453.43,  
                    1058736.73, 
                    1286847.69, 
                    988839.74,
                    641308.28,  
                    1261751.35, 
                    2597087.39]
         }]
    });

$('#ambiente2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '17 - Ambiente - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 738551.74,
                    2689881.56,
                    4049999.78,
                    4243051.16,
                    2302840.94,
                    655888.51,
                    4326497.68,
                    899018.33,
                    2188200.45,
                    4041728.78,
                    479435.38,
                    4590365.76]
         }]
    });

 $('#ambiente2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '17 - Ambiente - 2015'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2015'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 1227013.09, 
                    2407389.50, 
                    2862421.54, 
                    2673155.86, 
                    2646953.68, 
                    898755.48,  
                    4839412.24, 
                    3097830.48, 
                    2711060.95, 
                    2385530.86, 
                    2705010.35, 
                    5458972.20]
         }]
    });

 $('#vivienda2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '19 - Vivienda y Desarrollo Urbano - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 738551.74,
                    2689881.56,
                    4049999.78,
                    4243051.16,
                    2302840.94,
                    655888.51,
                    4326497.68,
                    899018.33,
                    2188200.45,
                    4041728.78,
                    479435.38,
                    4590365.76]
         }]
    });

 $('#vivienda2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '19 - Vivienda y Desarrollo Urbano - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 738551.74,
                    2689881.56,
                    4049999.78,
                    4243051.16,
                    2302840.94,
                    655888.51,
                    4326497.68,
                    899018.33,
                    2188200.45,
                    4041728.78,
                    479435.38,
                    4590365.76]
         }]
    });

$('#salud2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '20 - Salud - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 91248.73,
                    73003.80,
                    79289.09,   
                    103472.16,  
                    99566.73,   
                    108718.52,  
                    161403.16,  
                    106300.95,  
                    125615.47,  
                    170495.71,  
                    113710.31,  
                    153672.26,]
         }]
    });

 $('#salud2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '20 - Salud - 2015'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2015'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 83066.83,   
                    56475.58,   
                    75779.30,   
                    64239.11,   
                    75620.05,   
                    79906.94,   
                    100990.22,  
                    92818.89,   
                    104280.65,  
                    87437.37,   
                    101552.46,  
                    189214.72]
         }]
    });

 $('#cultura2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '21 - Cultura y Deporte - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 168438.42,  
                    210906.87,  
                    235833.36,  
                    276711.97,  
                    218797.11,  
                    281997.76,  
                    389995.36,  
                    214027.63,  
                    211559.44,  
                    254919.42,  
                    333669.53,  
                    392035.82]
         }]
    });

  $('#cultura2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '21 - Cultura y Deporte - 2015'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2015'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 151991.08,  
                    133103.44,  
                    209179.34,  
                    394454.83,  
                    440512.73,  
                    276593.78,  
                    591134.63,  
                    360600.35,  
                    300648.96,  
                    326473.37,  
                    958199.18,  
                    1654609.54  ]
         }]
    });

$('#proteccion2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Protección Social - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 223574.04,  
                    231743.42,  
                    254988.11,  
                    279241.88,  
                    237034.62,  
                    275222.32,  
                    324950.75,  
                    285565.61,  
                    221264.43,  
                    328176.01,  
                    310039.42,  
                    459023.53]
         }]
    });

$('#proteccion2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '23 - Protección Social - 2015'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2015'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [     184030.14,  
                      185551.05,    
                      273505.96,    
                      248999.10,    
                      291840.23,    
                      290408.70,    
                      377330.63,    
                      258921.45,    
                      295034.98,    
                      315278.66,    
                      345706.04,    
                      845242.22 ]
         }]
    });

 $('#prevision2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '24 - Previsión Social - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 421195.07,  
                    444556.67,  
                    433949.26,  
                    431520.59,  
                    506011.03,  
                    439148.80,  
                    851253.07,  
                    413289.34,  
                    413047.96,  
                    436370.04,  
                    400983.86,  
                    858868.19]
         }]
    });

$('#prevision2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: '24 - Previsión Social - 2014'
        },
        xAxis: {
                max: 11,
            categories: [                       
            'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
            'Noviembre',
            'Diciembre']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Devengado Mensual - 2014'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'decimal'
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 421195.07,  
                    444556.67,  
                    433949.26,  
                    431520.59,  
                    506011.03,  
                    439148.80,  
                    851253.07,  
                    413289.34,  
                    413047.96,  
                    436370.04,  
                    400983.86,  
                    858868.19]
         }]
    });

$('#detalle2014').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Detalle Presupuestal 2014'
        },
        xAxis: {
            max: 9,
            categories: [
                    'Plan, Gest y Res de Contingencia',
                  'Orden Pub Y Seg',
                  'Comercio',
                  'Transporte',
                  'Ambiente',
                  'Viv y Des Urbano',
                  'Salud',
                  'Cultura Y Deporte',
                  'Protección Social',
                  'Prevision Social']
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'FUNCIONES 2014'
            }
        }, {
            title: {
                text: 'Profit (millions)'
            },
            opposite: true
        }],
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },
        series: [{
            name: 'PIM',
            color: 'rgba(248,161,63,1)',
            data: [ 82910130.00,
                    37374735.00,
                    9000367.00,
                    29038953.00,
                    40961748.00,
                    10563443.00,
                    1540946.00,
                    3327357.00,
                    3743661.00,
                    6361000.00],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0,
            pointPlacement: 0,
            yAxis: 1
        },{
            name: 'UE',
            color: 'rgba(46, 204, 113,.9)',
            data: [ 73148138.36,
                    29447922.91,
                    7926063.05,
                    14399100.69,
                    31205460.07,
                    8166960.78,
                    1386496.89,
                    3188892.69,
                    3430824.14,
                    6245859.17],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0,
            pointPlacement: 0,
            yAxis: 1
        },{
            name: 'PIA',
            color: 'rgba(52, 152, 219,.5)',
            data: [ 47749395,
                    26320370,
                    8075022.10,
                    38950289,
                    26456212,
                    5303113,
                    1090837.00,
                    1951587,
                    2586340,
                    6361000],
             tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0,
            pointPlacement: 0,
            yAxis: 1
        }]
    });

    $('#detalle2015').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Detalle Presupuestal 2015'
        },
        subtitle: {
            text: '<p><b>PIA:</b>  S/.195,224,052.59</p></br> - <p><b>PIM:</b> S/. 211,389,121.00</p></br>  - <p><b>Ejec:</b>  S/. 168,842,394.96</p> ' 
        },
        xAxis: {
            max: 9,
            categories: [
                    'Plan, Gest y Res de Contingencia',
                  'Orden Pub Y Seg',
                  'Comercio',
                  'Transporte',
                  'Ambiente',
                  'Viv y Des Urbano',
                  'Salud',
                  'Cultura Y Deporte',
                  'Protección Social',
                  'Prevision Social']
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'FUNCIONES 2015'
            }
        }, {
            title: {
                text: 'En Millones de Soles'
            },
            opposite: true
        }],
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },
        series: [{
            name: 'PIM',
            color: 'rgba(248,161,63,1)',
            data: [ 79596212.00,
                    33137522.00,
                    7661476.00,
                    20593060.00,
                    37601852.00,
                    10653852.00,
                    2022335.00,
                    9180411.00,
                    4530581.00,
                    6411820.00],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.3,
            pointPlacement: -0.4,
            yAxis: 1
        },{
            name: 'Devengado',
            color: 'rgba(46, 204, 113,.9)',
            data: [ 62003128.04,
                    28694653.74,
                    7062013.96,
                    11649252.75,
                    33913506.23,
                    8648913.85,
                    1111382.12,
                    5797501.23,
                    3911849.16,
                    6050193.88],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.3,
            pointPlacement: -0.1,
            yAxis: 1
        },{
            name: 'PIA',
            color: 'rgba(52, 152, 219,.5)',
            data: [ 61326096.00,
                    27758035.00,
                    5519866.00,
                    33935916.00,
                    28711333.00,
                    6228673.00,
                    977879.00,
                    1944364.00,
                    2930351.00,
                    6361000.00],
             tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.3,
            pointPlacement: 0.2,
            yAxis: 1
        }]
    });

    $('#detalle2016').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Detalle Presupuestal 2016'
        },
        subtitle: {
            text: '<p><b>PIA:</b>  S/.115,910,611.63</p></br> - <p><b>PIM:</b> S/. 210,812,997.00</p></br>  - <p><b>Ejec:</b>  S/. 17,196,511.71</p> ' 
        },
        xAxis: {
            max: 9,
            categories: [
                    'Plan, Gest y Res de Contingencia',
                  'Orden Pub Y Seg',
                  'Comercio',
                  'Transporte',
                  'Ambiente',
                  'Viv y Des Urbano',
                  'Salud',
                  'Cultura Y Deporte',
                  'Protección Social',
                  'Prevision Social']
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'FUNCIONES 2016'
            }
        }, {
            title: {
                text: 'En Millones de Soles'
            },
            opposite: true
        }],
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },
        series: [{
            name: 'PIM',
            color: 'rgba(248,161,63,1)',
            data: [  70923229.00, 
                     39747267.00,
                     7980055.00, 
                     21340586.00, 
                     41494269.00,
                     8736733.00, 
                     2371705.00, 
                     7551275.00, 
                     4612325.00, 
                     6055553.00],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.3,
            pointPlacement: -0.4,
            yAxis: 1
        },{
            name: 'Devengado',
            color: 'rgba(46, 204, 113,.9)',
            data: [ 5972011.34,
                    3455015.14,
                    1128724.80,
                    1373678.87,
                    1624122.68,
                    1128259.68,
                    194253.60,
                    1029545.41,
                    458879.62,
                    832020.57],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.3,
            pointPlacement: -0.1,
            yAxis: 1
        },{
            name: 'PIA',
            color: 'rgba(52, 152, 219,.5)',
            data: [ 37107007.35,
                    22455124.94,
                    7257858.70,
                    12355546.00,
                    14720407.35,
                    7418823.75,
                    1240807.08,
                    4481268.36,
                    3165404.48,
                    5708363.62],
             tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.3,
            pointPlacement: 0.2,
            yAxis: 1
        }]
    });



//CHARTS POR FUNCIONES 2015 - TIPO AREA

$('#area-plan2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Plan. Gest y Res. de Contingencia - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.62,003,128.04</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [     2295611.19, 
                    2862788.65, 
                    3677816.84, 
                    3600623.16, 
                    3615867.11, 
                    3749899.21, 
                    4168769.49, 
                    3459573.22, 
                    14779999.42,    
                    3434804.58,
                    4008598.90, 
                    12348776.27]
        }]
    });

$('#area-seguridad2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Orden Público y Seguridad - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.28,694,653.74</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 1388795.17, 
                    1917165.87, 
                    1962108.20, 
                    1914619.57, 
                    2086334.70, 
                    1982075.32, 
                    2493834.93, 
                    2499135.80,
                    2726243.04, 
                    2098918.30, 
                    3363269.32, 
                    4262153.52]
        }]
    });

$('#area-comercio2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Comercio - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.7,062,013.96</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 628682.93,  
                    493847.27,  
                    588113.13,  
                    552807.96,  
                    570349.63,  
                    482334, 
                    664308.33,  
                    489266.78,  
                    511725.49,  
                    532054.30,  
                    676193.78,  
                    872330.36]
        }]
    });

$('#area-transporte2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Transporte - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.11,649,252.75</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 393313.67,  
                    595560.22,  
                    642560.01,  
                    846832.57,  
                    875961.67,  
                    460453.43,  
                    1058736.73, 
                    1286847.69, 
                    988839.74,
                    641308.28,  
                    1261751.35, 
                    2597087.39]
        }]
    });

$('#area-ambiente2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Ambiente - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.33,913,506.23</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [738551.74,
                    2689881.56,
                    4049999.78,
                    4243051.16,
                    2302840.94,
                    655888.51,
                    4326497.68,
                    899018.33,
                    2188200.45,
                    4041728.78,
                    479435.38,
                    4590365.76]
        }]
    });

$('#area-vivienda2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Vivienda y Desarrollo Urbano - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.8,648,913.85</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 836333.22,
                    516172, 
                    757811.76,  
                    1034610.33, 
                    485828.78,
                    568964.34,  
                    748139.71,  
                    808857.09,  
                    610475.89,  
                    574100.84,  
                    684174.79,  
                    1023445.10]
        }]
    });


$('#area-salud2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Salud - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.1,111,382.12</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en <strong>MILES</strong> de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 83066.83,   
                    56475.58,   
                    75779.30,   
                    64239.11,   
                    75620.05,   
                    79906.94,   
                    100990.22,  
                    92818.89,   
                    104280.65,  
                    87437.37,   
                    101552.46,  
                    189214.72]
        }]
    });


$('#area-cultura2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Cultura y Deporte - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.5,797,501.23</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en <strong>MILES</strong> de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 151991.08,  
                    133103.44,  
                    209179.34,  
                    394454.83,  
                    440512.73,  
                    276593.78,  
                    591134.63,  
                    360600.35,  
                    300648.96,  
                    326473.37,  
                    958199.18,  
                    1654609.54]
        }]
    });

$('#area-proteccion2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Protección Social - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.3,911,849.16</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 184030.14,  
                      185551.05,    
                      273505.96,    
                      248999.10,    
                      291840.23,    
                      290408.70,    
                      377330.63,    
                      258921.45,    
                      295034.98,    
                      315278.66,    
                      345706.04,    
                      845242.22]
        }]
    });

$('#area-prevision2015').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Previsión Social - 2015'
        },
        subtitle: {
            text: '<b>Total Ejecutado: S/.6,050,193.88</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en <strong>MILES</strong> de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 421195.07,  
                    444556.67,  
                    433949.26,  
                    431520.59,  
                    506011.03,  
                    439148.80,  
                    851253.07,  
                    413289.34,  
                    413047.96,  
                    436370.04,  
                    400983.86,  
                    858868.19]
        }]
    });

//CHARTS POR FUNCIONES 2016 - TIPO AREA

$('#area-plan2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Plan. Gest y Res. de Contingencia - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.5,972,011.34</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 2488641.17,
                  1584043.16,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00]
        }]
    });

$('#area-seguridad2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Orden Público y Seguridad - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.3,455,015.14</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 1584043.16, 
                    1870971.98, 
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,
                    0,  
                    0,  
                    0,  
                    0]
        }]
    });

$('#area-comercio2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Comercio 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.1,128,724.80</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 607695.06,  
                    521029.74,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0 ]
        }]
    });

$('#area-transporte2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Transporte - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.1,373,678.87</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 578745.31,  
                    794933.56,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,
                    0,  
                    0,  
                    0]
        }]
    });

$('#area-ambiente2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Ambiente - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.1,624,122.68</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 720473.57,  
                    903649.11,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0]
        }]
    });

$('#area-vivienda2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Vivienda y Desarrollo Urbano - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.1,128,259.68</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [     570596.7,   
                    557662.98,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0]
        }]
    });

$('#area-salud2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Salud - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.194.253.60</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 102966.30,  
                    91287.30,   
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0]
        }]
    });

$('#area-cultura2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Cultura y Deporte - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.1,029,545.41</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 274709.12,  
                    754836.29,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0]
        }]
    });

$('#area-proteccion2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Protección Social - 2016'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.458,879.62</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Miles de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [     228309.20,  
                    230570.42,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0]
        }]
    });

$('#area-prevision2016').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Previsón Social - 2015'
        },
        subtitle: {
            text: '<b>Ejecutado a la Fecha: S/.832,020.57</b><br>Fuente Primaria - Mun. San Isidro'
        },
        xAxis: {
            max: 11,
            categories: [                       
            'Ene',
                        'Feb',
                        'Mar',
                        'Abr',
                        'May',
                        'Jun',
                        'Jul',
                        'Ago',
                        'Sep',
                        'Oct',
            'Nov',
            'Dic']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Expresado en Millones de Soles'
            }
            },
        tooltip: {
            pointFormat: '{series.name}<b> S/.{point.y}</b><br/>'            
        },
        plotOptions: {
            area: {
                pointStart: 0,
                stacking: 'normal',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Presupuesto Ejecutado',
            data: [ 401969.99,  
                    430050.58,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0]
        }]
    });

//SALDOS PRESUPUESTALES 

$('#saldo2015').highcharts({

        chart: {
            type: 'columnrange',
            inverted: false,
            zoomType: 'xy'
        },

        title: {
            text: 'Saldo Presupuestal 2015'
        },

        subtitle: {
            text: 'Resultado de la diferencia entre el PIM y el UE (Ejecutado)'
        },

        xAxis: {
            categories: ['Plan, Gest y Res de Contingencia',
                  'Orden Pub Y Seg',
                  'Comercio',
                  'Transporte',
                  'Ambiente',
                  'Viv y Des Urbano',
                  'Salud',
                  'Cultura Y Deporte',
                  'Protección Social',
                  'Prevision Social']
        },

        yAxis: {
            title: {
                text: 'Exp. en Miles y Millones de Soles'
                
            }
        },

        tooltip: {
         pointFormat: '{point.name.x}'
           
        },

        plotOptions: {
            
        },

        legend: {
            enabled: false
        },

        series: [{
            name: 'Saldo',
            data: [
                 [62003128.04, 79596212.00],
                 [28694653.74, 33137522.00],
                 [7062013.96, 7661476.00 ],
                 [11649252.75, 20593060.00 ],
                 [33913506.23, 37601852.00 ],
                 [8648913.85, 10653852.00 ],
                 [1111382.12, 2022335.00],
                 [5797501.23, 9180411.00], 
                 [3911849.16, 4530581.00], 
                 [6050193.88, 6411820.00]
            ],

             tooltip: {
                    enabled: false
        },
            
        },
        
        {
            name: 'Saldo',
            type: 'scatter',
            data: [
                   ["S/. 17,593,083.96", 70799670.02 ],
                   ["S/. 4,442,868.26 ",30916087.87], 
                   ["S/. 599,462.04 ",7361744.98], 
                   ["S/. 8,943,807.25 ",16121156.38], 
                   ["S/. 3,688,345.77 ",35757679.12],
                   ["S/. 2,004,938.15 ",9651382.93], 
                   ["S/. 910,952.88 ",1566858.56], 
                   ["S/. 3,382,909.77 ",7488956.12], 
                   ["S/. 618,731.84 ",4221215.08], 
                   ["S/. 361,626.12 ",6231006.94]
                   ],
            color: 'transparent',
           tooltip: {
              pointFormat: '{point.name}',
              style: '{font-size:05px, color:red}' 
        }
        }]
    });

// COMPARACION DEVENGADO Y EJECUACIÓN MENDUAL POR FUNCIONES

$('#compara-plan1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [         2295611.19, 
                    2862788.65, 
                    3677816.84, 
                    3600623.16, 
                    3615867.11, 
                    3749899.21, 
                    4168769.49, 
                    3459573.22, 
                    14779999.42,    
                    3434804.58,
                    4008598.90, 
                    12348776.27
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [ 2488641.17,
                  1584043.16,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00,
                  0.00
                    ]
        }]
    });

$('#compara-seguridad1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 1388795.17, 
                    1917165.87, 
                    1962108.20, 
                    1914619.57, 
                    2086334.70, 
                    1982075.32, 
                    2493834.93, 
                    2499135.80,
                    2726243.04, 
                    2098918.30, 
                    3363269.32, 
                    4262153.52
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [1584043.16,  
                    1870971.98, 
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,
                    0,  
                    0,  
                    0,  
                    0
                    ]
        }]
    });

$('#compara-comercio1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 628682.93,  
                    493847.27,  
                    588113.13,  
                    552807.96,  
                    570349.63,  
                    482334, 
                    664308.33,  
                    489266.78,  
                    511725.49,  
                    532054.30,  
                    676193.78,  
                    872330.36
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [607695.06,   
                    521029.74,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0 
                    ]
        }]
    });

$('#compara-transporte1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 393313.67,  
                    595560.22,  
                    642560.01,  
                    846832.57,  
                    875961.67,  
                    460453.43,  
                    1058736.73, 
                    1286847.69, 
                    988839.74,
                    641308.28,  
                    1261751.35, 
                    2597087.39
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [578745.31,   
                    794933.56,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,
                    0,  
                    0,  
                    0
                    ]
        }]
    });

$('#compara-ambiente1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 738551.74,
                    2689881.56,
                    4049999.78,
                    4243051.16,
                    2302840.94,
                    655888.51,
                    4326497.68,
                    899018.33,
                    2188200.45,
                    4041728.78,
                    479435.38,
                    4590365.76
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [720473.57,   
                    903649.11,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0
                    ]
        }]
    });

$('#compara-vivienda1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 836333.22,
                    516172, 
                    757811.76,  
                    1034610.33, 
                    485828.78,
                    568964.34,  
                    748139.71,  
                    808857.09,  
                    610475.89,  
                    574100.84,  
                    684174.79,  
                    1023445.10
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [ 570596.7,   
                    557662.98,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0
                    ]
        }]
    });

$('#compara-salud1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [     83066.83,   
                    56475.58,   
                    75779.30,   
                    64239.11,   
                    75620.05,   
                    79906.94,   
                    100990.22,  
                    92818.89,   
                    104280.65,  
                    87437.37,   
                    101552.46,  
                    189214.72
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [102966.30,   
                    91287.30,   
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0
                    ]
        }]
    });

$('#compara-cultura1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 151991.08,  
                    133103.44,  
                    209179.34,  
                    394454.83,  
                    440512.73,  
                    276593.78,  
                    591134.63,  
                    360600.35,  
                    300648.96,  
                    326473.37,  
                    958199.18,  
                    1654609.54
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [274709.12,   
                    754836.29,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0
                    ]
        }]
    });

$('#compara-proteccion1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [ 184030.14,  
                      185551.05,    
                      273505.96,    
                      248999.10,    
                      291840.23,    
                      290408.70,    
                      377330.63,    
                      258921.45,    
                      295034.98,    
                      315278.66,    
                      345706.04,    
                      845242.22
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [     228309.20,  
                    230570.42,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0
                    ]
        }]
    });

$('#compara-prevision1516').highcharts({
        chart: {
            zoomType: 'xy'
        },        
        title: {
            text: 'Comparación entre los años 2015 y 2016',
            x: 0 //center
        },
        subtitle: {
            text: 'Presupuesto Ejecutado (Devengado)<br>Selecciona un área con el mouse, para hacer Zoom',
            x: 0
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: 'Expresado en MM de Soles'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: 'S/.'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2015',
            data: [421195.07,   
                    444556.67,  
                    433949.26,  
                    431520.59,  
                    506011.03,  
                    439148.80,  
                    851253.07,  
                    413289.34,  
                    413047.96,  
                    436370.04,  
                    400983.86,  
                    858868.19
                    ]
        }, {
            name: '2016',
            color: 'rgba(230, 126, 34,1.0)',
            data: [401969.99,   
                    430050.58,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0,  
                    0
                    ]
        }]
    });

// COMPARACIÓN DETALLE PRESUPUESTAL PIM - PIA - UE

$('#compara-detalle1516').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Comparativo PIM-PIA 2015-2016'
        },
        subtitle: {
            text: '<p><b>PIM-2015:</b>  S/.211,389,121.00</p> - <p><b>PIA-2015:</b>  S/.175,693,513.00</p><br><p><b>PIM-2016:</b> S/.210,812,997.00</p> - <p><b>PIA-2016:</b>  S/.115,910,611.63</p><br><p>Selecciona un área con el mouse para hacer zoom en el cuadro.</p>' 
        },
        xAxis: {
            max: 9,
            categories: [
                    'Plan, Gest y Res de Contingencia',
                  'Orden Pub Y Seg',
                  'Comercio',
                  'Transporte',
                  'Ambiente',
                  'Viv y Des Urbano',
                  'Salud',
                  'Cultura Y Deporte',
                  'Protección Social',
                  'Prevision Social']
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'FUNCIONES 2016'
            }
        }, {
            title: {
                text: 'En Millones de Soles'
            },
            opposite: true
        }],
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },
        series: [{
            name: 'PIM-2015',
            color: 'rgba(241, 196, 15,0.8)',
            data: [  79596212.00,
                    33137522.00,
                    7661476.00,
                    20593060.00,
                    37601852.00,
                    10653852.00,
                    2022335.00,
                    9180411.00,
                    4530581.00,
                    6411820.00],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.2,
            pointPlacement: -0.2,
            yAxis: 1
        },{
            name: 'PIA-2015',
            color: 'rgba(52, 152, 219,0.7)',
            data: [ 61326096.00,
                    27758035.00,
                    5519866.00,
                    33935916.00,
                    28711333.00,
                    6228673.00,
                    977879.00,
                    1944364.00,
                    2930351.00,
                    6361000.00],
             tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.2,
            pointPlacement: -0.2,
            yAxis: 0
        },{
            name: 'PIM-2016',
            color: 'rgba(230, 126, 34,0.8)',
            data: [ 70923229.00, 
                     39747267.00,
                     7980055.00, 
                     21340586.00, 
                     41494269.00,
                     8736733.00, 
                     2371705.00, 
                     7551275.00, 
                     4612325.00, 
                     6055553.00],
            tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.2,
            pointPlacement: 0.2,
            yAxis: 1
        },{
            name: 'PIA-2016',
            color: 'rgba(41, 128, 185,0.7)',
            data: [ 37107007.35,
                    22455124.94,
                    7257858.70,
                    12355546.00,
                    14720407.35,
                    7418823.75,
                    1240807.08,
                    4481268.36,
                    3165404.48,
                    5708363.62],
             tooltip: {
                valuePrefix: 'S/.',
                valueSuffix: ' M'
            },
            pointPadding: 0.2,
            pointPlacement: 0.2,
            yAxis: 1
        }]
    });


    $('.presupuestotabInput').click(function() {

        if($('#mensual2014').length > 0) $('#mensual2014').highcharts().reflow();
        if($('#mensual2015').length > 0) $('#mensual2015').highcharts().reflow();
        if($('#comparar_2015_2016').length > 0) $('#comparar_2015_2016').highcharts().reflow();
        if($('#mensual2016').length > 0) $('#mensual2016').highcharts().reflow();
        if($('#anual2014').length > 0) $('#anual2014').highcharts().reflow();
        if($('#anual2015').length > 0) $('#anual2015').highcharts().reflow();
        if($('#anual2016').length > 0) $('#anual2016').highcharts().reflow();
        if($('#funciones2014').length > 0) $('#funciones2014').highcharts().reflow();
        if($('#comp1415').length > 0) $('#comp1415').highcharts().reflow();
        if($('#plan2014').length > 0) $('#plan2014').highcharts().reflow();
        if($('#plan2015').length > 0) $('#plan2015').highcharts().reflow();
        if($('#plan2016').length > 0) $('#plan2016').highcharts().reflow();
        if($('#seguridad2014').length > 0) $('#seguridad2014').highcharts().reflow();
        if($('#seguridad2015').length > 0) $('#seguridad2015').highcharts().reflow();
        if($('#comercio2014').length > 0) $('#comercio2014').highcharts().reflow();
        if($('#comercio2015').length > 0) $('#comercio2015').highcharts().reflow();
        if($('#transporte2014').length > 0) $('#transporte2014').highcharts().reflow();
        if($('#transporte2015').length > 0) $('#transporte2015').highcharts().reflow();
        if($('#ambiente2014').length > 0) $('#ambiente2014').highcharts().reflow();
        if($('#ambiente2015').length > 0) $('#ambiente2015').highcharts().reflow();
        if($('#vivienda2014').length > 0) $('#vivienda2014').highcharts().reflow();
        if($('#vivienda2015').length > 0) $('#vivienda2015').highcharts().reflow();
        if($('#salud2014').length > 0) $('#salud2014').highcharts().reflow();
        if($('#salud2015').length > 0) $('#salud2015').highcharts().reflow();    
        if($('#cultura2014').length > 0) $('#cultura2014').highcharts().reflow();
        if($('#cultura2015').length > 0) $('#cultura2015').highcharts().reflow();
        if($('#prevision2014').length > 0) $('#prevision2014').highcharts().reflow();
        if($('#prevision2015').length > 0) $('#prevision2015').highcharts().reflow();
        if($('#proteccion2014').length > 0) $('#proteccion2014').highcharts().reflow();
        if($('#proteccion2015').length > 0) $('#proteccion2015').highcharts().reflow();
        if($('#detalle2014').length > 0) $('#detalle2014').highcharts().reflow();
        if($('#detalle2015').length > 0) $('#detalle2015').highcharts().reflow();
        if($('#detalle2016').length > 0) $('#detalle2016').highcharts().reflow();
        if($('#area-plan2015').length > 0) $('#area-plan2015').highcharts().reflow();
        if($('#area-comercio2015').length > 0) $('#area-comercio2015').highcharts().reflow();
        if($('#area-transporte2015').length > 0) $('#area-transporte2015').highcharts().reflow();
        if($('#area-ambiente2015').length > 0) $('#area-ambiente2015').highcharts().reflow();
        if($('#area-vivienda2015').length > 0) $('#area-vivienda2015').highcharts().reflow();
        if($('#area-salud2015').length > 0) $('#area-salud2015').highcharts().reflow();
        if($('#area-cultura2015').length > 0) $('#area-cultura2015').highcharts().reflow();
        if($('#area-proteccion2015').length > 0) $('#area-proteccion2015').highcharts().reflow();
        if($('#area-prevision2015').length > 0) $('#area-prevision2015').highcharts().reflow();
        if($('#area-plan2016').length > 0) $('#area-plan2016').highcharts().reflow();
        if($('#area-seguridad2016').length > 0) $('#area-seguridad2016').highcharts().reflow();
        if($('#area-comercio2016').length > 0) $('#area-comercio2016').highcharts().reflow();
        if($('#area-transporte2016').length > 0) $('#area-transporte2016').highcharts().reflow();
        if($('#area-ambiente2016').length > 0) $('#area-ambiente2016').highcharts().reflow();
        if($('#area-vivienda2016').length > 0) $('#area-vivienda2016').highcharts().reflow();
        if($('#area-salud2016').length > 0) $('#area-salud2016').highcharts().reflow();
        if($('#area-cultura2016').length > 0) $('#area-cultura2016').highcharts().reflow();
        if($('#area-proteccion2016').length > 0) $('#area-proteccion2016').highcharts().reflow();
        if($('#area-prevision2016').length > 0) $('#area-prevision2016').highcharts().reflow();
        if($('#saldo2015').length > 0) $('#saldo2015').highcharts().reflow();
        if($('#compara-plan1516').length > 0) $('#compara-plan1516').highcharts().reflow();
        if($('#compara-seguridad1516').length > 0) $('#compara-seguridad1516').highcharts().reflow();
        if($('#compara-comercio1516').length > 0) $('#compara-comercio1516').highcharts().reflow();
        if($('#compara-transporte1516').length > 0) $('#compara-transporte1516').highcharts().reflow();
        if($('#compara-ambiente1516').length > 0) $('#compara-ambiente1516').highcharts().reflow();
        if($('#compara-vivienda1516').length > 0) $('#compara-vivienda1516').highcharts().reflow();
        if($('#compara-salud1516').length > 0) $('#compara-salud1516').highcharts().reflow();
        if($('#compara-cultura1516').length > 0) $('#compara-cultura1516').highcharts().reflow();
        if($('#compara-proteccion1516').length > 0) $('#compara-proteccion1516').highcharts().reflow();
        if($('#compara-prevision1516').length > 0) $('#compara-prevision1516').highcharts().reflow();
        if($('#compara-detalle1516').length > 0) $('#compara-detalle1516').highcharts().reflow();

        






        //$('#mensual2016').highcharts().reflow();

            
    });


});