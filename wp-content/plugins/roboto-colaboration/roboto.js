jQuery( function( $ ) {

	$('#roboto-vote').click(function(event) {
		event.preventDefault();
		var canvote = $(this).attr('data-canvote');

		if(canvote==1) {

			$('#roboto-vote span').html('Cargando...');

			$.post(
				roboajax.url,
				{
					action: 'roboto',
					postid: $(this).attr('data-post'),
					wpnonce: roboajax.wpnonce
				},
				function(response) {
					$('#roboto-vote span').hide().html(response).fadeIn('fast');
			});
		}
	});


	$('#button_more_cards').click(function(event) {

		var numpost = $('#post_cards').find('.card').length;

		$('#morecards_loading').slideDown('fast');

		$.post(
			roboajax.url,
			{
				action: 'morecards',
				inipost: numpost,
				wpnonce: roboajax.wpnonce
			},
			function(response) {
				$('#morecards_loading').slideUp('fast');
				$('#post_cards').append(response).slideDown('slow');
		});
	});

	$('#roboto_submit').click(function(event) {
		$(this).html('Enviando...');
	});

});