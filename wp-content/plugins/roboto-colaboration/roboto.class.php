<?php
	class roboto {

		protected $array_usersid_vote = array();
		protected $the_post_id;
		//protected $array_topic = array('Seguridad Ciudadana','Servicios Municipales','Desarrollo Social','Estilo de vida', 'Otros');

		function __construct() {
			add_action( 'init', array($this,'add_process_init'));
			add_action( 'wp_enqueue_scripts', array($this, 'addscripts' ));

			add_action( 'admin_footer-post.php', array($this,'add_post_status'));
			add_filter( 'display_post_states', array($this,'add_list_status'));

			add_action( 'wp_ajax_roboto', array($this, 'add_vote') );
			add_action( 'wp_ajax_nopriv_roboto', array($this, 'add_vote') );

			add_action( 'wp_ajax_morecards', array($this, 'more_cards') );
			add_action( 'wp_ajax_nopriv_morecards', array($this, 'more_cards') );

			add_action('register_form',array($this,'set_register_form'), 5);
			add_action('user_register', array($this,'save_register_form'));
			//add_action('register_post',array($this,'error_register_form'),10,3);
			add_filter('registration_errors', array($this,'error_register_form'),10,3);
			add_action('login_form_register', array($this,'hack_register_form'));

			add_action('login_head', array($this,'login_head'));
		}

		function addscripts() {

			//wp_enqueue_style('roboto-css', plugins_url('/templates/roboto.css', __FILE__ ) );
			wp_enqueue_script('roboto-js', plugins_url('/roboto.js',__FILE__), array('jquery'),false,true);
			wp_localize_script('roboto-js', 'roboajax', array('url' => admin_url('admin-ajax.php'), 'wpnonce' => wp_create_nonce( 'roboto-wpnonce' ) ));

			if(is_page('transparencia-2')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);

			}

			if(is_page('presupuesto-anual')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
				
			}


			if(is_page('presupuesto-mensual')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
				
			}

			if(is_page('presupuesto-mensual-2014')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('presupuesto-mensual-2015')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('comparativo-2014-2015')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('presupuesto-funciones')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('planeamiento')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('seguridad')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('comercio')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('transporte')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('ambiente')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('desarrollo-vivienda')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('salud')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('cultura')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('proteccion-social')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('prevision-social')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-exporting', 'https://code.highcharts.com/modules/exporting.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('detalle')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}

			if(is_page('saldos')) {
				wp_enqueue_script('highchart-js', 'https://code.highcharts.com/highcharts.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-more-js', 'https://code.highcharts.com/highcharts-more.js', array('jquery'),false,true);
				wp_enqueue_script('highchart-data', 'https://code.highcharts.com/modules/data.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-exporting', 'https://code.highcharts.com/modules/exporting.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-drill', 'https://code.highcharts.com/modules/drilldown.js', array('jquery','highchart-js'),false,true);
				wp_enqueue_script('highchart-script', plugins_url('/roboto-transparencia.js',__FILE__), array('jquery','highchart-js','highchart-data','highchart-drill'),false,true);
			}
		}


		function add_process_init() {

			register_taxonomy( 'area',
								array( 'proyecto' ),
								array(
									'labels' => array(
											'name'              => __( 'Area', 'roboto' ),
											'singular_name'     => __( 'Area', 'roboto' ),
											'search_items'      => __( 'Buscar areas', 'roboto' ),
											'all_items'         => __( 'Todas las areas', 'roboto' ),
											'edit_item'         => __( 'Editar area', 'roboto' ),
											'update_item'       => __( 'Actualizar area', 'roboto' ),
											'add_new_item'      => __( 'Agregar nueva area', 'roboto' ),
											'new_item_name'     => __( 'Agregar nueva area', 'roboto' ),
											'menu_name'         => __( 'Area', 'roboto' )
										),
									'hierarchical'      => true,
									'show_ui'           => true,
									'show_admin_column' => true,
									'query_var'         => true,
									'rewrite'           => array( 'slug' => 'area' )
								)
							);


			register_taxonomy( 'tipo-impacto',
								array( 'proyecto' ),
								array(
									'labels' => array(
											'name'              => __( 'Tipo de Impacto', 'roboto' ),
											'singular_name'     => __( 'Tipo de Impacto', 'roboto' ),
											'search_items'      => __( 'Buscar Tipos de Impactos', 'roboto' ),
											'all_items'         => __( 'Todas los Tipo de Impacto', 'roboto' ),
											'edit_item'         => __( 'Editar Tipo de Impacto', 'roboto' ),
											'update_item'       => __( 'Actualizar Tipo de Impacto', 'roboto' ),
											'add_new_item'      => __( 'Agregar nuevo Tipo de Impacto', 'roboto' ),
											'new_item_name'     => __( 'Agregar nuevo Tipo de Impacto', 'roboto' ),
											'menu_name'         => __( 'Tipo de Impacto', 'roboto' )
										),
									'hierarchical'      => true,
									'show_ui'           => true,
									'show_admin_column' => true,
									'query_var'         => true,
									'rewrite'           => array( 'slug' => 'tipo-impacto' )
								)
							);

			register_post_status('en-proceso',
									array('label' => 'En proceso',
											'label_count' => _n_noop( 'En proceso <span class="count">(%s)</span>', 'En proceso <span class="count">(%s)</span>'),
											'public' => true)
									);

			register_post_status('aprobado',
									array('label' => 'Aprobado',
											'label_count' => _n_noop( 'Aprobado <span class="count">(%s)</span>', 'Aprobados <span class="count">(%s)</span>'),
											'public' => true)
									);

			register_post_status('finalizado',
									array('label' => 'Finalizado',
											'label_count' => _n_noop( 'Finalizado <span class="count">(%s)</span>', 'Finalizados <span class="count">(%s)</span>'),
											'public' => true)
									);

			register_post_status('rechazado',
									array('label' => 'Rechazado',
											'label_count' => _n_noop( 'Rechazado <span class="count">(%s)</span>', 'Rechazados <span class="count">(%s)</span>'),
											'public' => true)
									);

			register_taxonomy( 'tema',
								array( 'proyecto' ),
								array(
									'labels' => array(
											'name'              => __( 'Tema', 'roboto' ),
											'singular_name'     => __( 'Temas', 'roboto' ),
											'search_items'      => __( 'Buscar Temas', 'roboto' ),
											'all_items'         => __( 'Todas los Temas', 'roboto' ),
											'edit_item'         => __( 'Editar Tema', 'roboto' ),
											'update_item'       => __( 'Actualizar Tema', 'roboto' ),
											'add_new_item'      => __( 'Agregar nuevo Tema', 'roboto' ),
											'new_item_name'     => __( 'Agregar nueva Tema', 'roboto' ),
											'menu_name'         => __( 'Tema', 'roboto' )
										),
									'hierarchical'      => true,
									'show_ui'           => true,
									'show_admin_column' => true,
									'query_var'         => true,
									'rewrite'           => array( 'slug' => 'tema' )
								)
							);


  			register_post_type( 'proyecto',
    									array(
      										'labels' => array(
        										'name'			=> __( 'Banco de Proyectos', 'roboto' ),
        										'singular_name'	=> __( 'Proyecto', 'roboto' ),
        										'add_new'		=> __( 'Agregar Proyecto', 'roboto' ),
      										),
      										'public' => true,
      										'publicly_queryable'	=> true,
      										'query_var'				=> true,
      										'has_archive'			=> true,
      										'rewrite'				=> array('slug' => 'proyecto'),
      										'capability_type'  		=> 'post',
      										'hierarchical'			=> false,
      										'supports'				=> array( 'title', 'editor', 'author', 'thumbnail', 'comments' )
    									)
  									);


  			/*	Insert proyecto	*/
  			if( is_user_logged_in() && isset($_POST['roboto_submit']) ) {

				$array_data = array(
									'img' => $_FILES['roboto_img'],
									'title' => $_POST['roboto_name'],
									'desc' => $_POST['roboto_desc'],
									'cat' => $_POST['roboto_category']
							);

				if($this->save_colaboration($array_data)) {

					$current_user = wp_get_current_user();

					$to = array($current_user->user_email);
					$subject = $message = 'Tu participacion pendiente de aprobar';
					$headers = 'Reply-to: GobAbierto <support@gopymes.pe>' . "\r\n";

					wp_mail( $to, $subject, $message, $headers);

					//$linkit = get_permalink($this->the_post_id);
					//wp_redirect($linkit);
					wp_redirect(site_url('gracias/'));
				} else
					wp_redirect(site_url('formulario/?msg=error'));

				die();
			}
		}


		/****************************************************************
			Agrega los estados en forma manual ya que WP no dispone de
			herramientas automáticas para este caso de los status.
		*****************************************************************/
		function add_post_status(){
     		global $post;
     		
     		$complete = '';
     		$label = '';
     			
     		if( $post->post_type == 'proyecto' ) {

          		if($post->post_status == 'aprobado'){
               		$complete = ' selected=\'selected\' ';
               		$label = '<span id=\'post-status-display\'> Aprobado</span>';
          		} else $complete='';

          		echo '<script>
          			jQuery(document).ready(function($){
               			$("select#post_status").append("<option value=\'aprobado\' '.$complete.'>Aprobado</option>");
               			$(".misc-pub-section label").append("'.$label.'");
          			});
          		</script>';

          		if($post->post_status == 'en-proceso'){
               		$complete = ' selected=\'selected\' ';
               		$label = '<span id=\'post-status-display\'> En Proceso</span>';
          		} else $complete='';

          		echo '<script>
          			jQuery(document).ready(function($){
               			$("select#post_status").append("<option value=\'en-proceso\' '.$complete.'>En Proceso</option>");
               			$(".misc-pub-section label").append("'.$label.'");
          			});
          		</script>';

          		if($post->post_status == 'finalizado'){
               		$complete = ' selected=\'selected\' ';
               		$label = '<span id=\'post-status-display\'> Finalizado</span>';
          		} else $complete='';

          		echo '<script>
          			jQuery(document).ready(function($){
               			$("select#post_status").append("<option value=\'finalizado\' '.$complete.'>Finalizado</option>");
               			$(".misc-pub-section label").append("'.$label.'");
          			});
          		</script>';


          		if($post->post_status == 'rechazado'){
               		$complete = ' selected=\'selected\' ';
               		$label = '<span id=\'post-status-display\'> Rechazado</span>';
          		} else $complete='';

          		echo '<script>
          			jQuery(document).ready(function($){
               			$("select#post_status").append("<option value=\'rechazado\' '.$complete.'>Rechazado</option>");
               			$(".misc-pub-section label").append("'.$label.'");
          			});
          		</script>';
     		}
		}

		function add_list_status( $states ) {
			global $post;
     		$arg = get_query_var( 'post_status' );
     		
     		if($arg != 'aprobado') {
          		if($post->post_status == 'aprobado'){
               		return array('Aprobado');
          		}
     		}

     		if($arg != 'en-proceso') {
          		if($post->post_status == 'en-proceso'){
               		return array('En proceso');
          		}
     		}

     		if($arg != 'finalizado') {
          		if($post->post_status == 'finalizado'){
               		return array('Finalizado');
          		}
     		}

     		if($arg != 'rechazado') {
          		if($post->post_status == 'rechazado'){
               		return array('Rechazado');
          		}
     		}

    		return $states;
		}

		/**********************
			IF user can vote
		***********************/
		function can_vote($user_id, $post_id) {

			if( is_user_logged_in() ) { //si esta logueado

				//traigo los usuarios que ya han votado esa publicacion
        		$array_users_votes = get_field('uservotes',$post_id);

        		update_option('pollo1',print_r($array_users_votes,true));

        		//guardo el ID de los usuarios que han votado
        		if( is_array($array_users_votes) && count($array_users_votes)>0 ) {

        			foreach($array_users_votes as $user_vote)
        				$this->array_usersid_vote[]= (string)(isset($user_vote['ID'])?$user_vote['ID']:0);
        		}


        		if( !in_array($user_id,$this->array_usersid_vote) )
        			return 1;
        		else
        			return 0;
			} else
				return 0;
		}



		/************************************
			agregar votos a una publicacion
		*************************************/
		function add_vote() {

			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'roboto-wpnonce' ) )
        		die ( 'Busted!');

			$current_user = wp_get_current_user();

        	$post_id = $_POST['postid'];
        	$user_id = $current_user->ID;

        	if(!isset($user_id) || !isset($post_id)) return 0;

        	//obtengo el numero de votos de la publicacion
			$numvotes = get_field('numvotes',$post_id);

        	if( $this->can_vote($user_id, $post_id) ) { //si el usuario puede votar

        		//traigo los usuarios que ya han votado esa publicacion
        		$array_users_votes = get_field('uservotes',$post_id);

				//actualizo el numero de votos, sumandole uno
        		update_field('numvotes',++$numvotes,$post_id);

        		//actualizo la lista de usuarios que han votado por esa publicacion
        		$new_users_votes = array_push($this->array_usersid_vote,(string)$user_id);
        		
        		//Actualizao el campo
        		update_field('uservotes',$this->array_usersid_vote,$post_id);
        	}

        	echo $numvotes.' voto(s)';
        	die();
		}

		/************************************
			quitar votos a una publicacion
		*************************************/
		function remove_vote() {

			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'roboto-wpnonce' ) )
        		die ( 'Busted!');

        	$post_id = $_POST['post_id'];
        	$user_id = $_POST['user_id'];

        	if(!isset($user_id) || !isset($post_id)) return 0;

        	$array_users_votes = get_post_meta($post_id,'users_votes',true);

        	// Si el usuario puede votar
			if( is_array($array_users_votes) && ($user_key = array_search($user_id, $array_users_votes)) !== false && is_user_logged_in() ) {

				//obtengo el numero de votos de la publicacion
				$num_votes = get_post_meta($post_id,'num_votes',true);

				//actualizo el numero de votos, sumandole uno
        		@update_post_meta($post_id,'num_votes',--$num_votes);

        		//actualizo la lista de usuarios que han votado por esa publicacion
    			unset($array_users_votes[$user_key]);
        		@update_post_meta($post_id,'users_votes',$array_users_votes);
        	
        		echo $numvote.' voto(s)';		
        	}

        	die();
		}


		function more_cards() {
			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'roboto-wpnonce' ) )
        		die ( 'Busted!');

        	$inipost = $_POST['inipost'];

			$args = array(
                    'post_type' => 'proyecto',
                    'post_status' => 'publish',
                    'posts_per_page' => 6,
                    'offset' => $inipost
                );

        	$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) {

				while ( $the_query->have_posts() ) { $the_query->the_post();
    		?>
            	<div class="card radius shadowDepth1" >
    				<div class="card__image border-tlr-radius">
                    	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('last', array('class' => 'img-responsive border-tlr-radius')); ?></a>
                	</div>

    				<div class="card__content card__padding">
                    	<div class="card__share icon-share-card">
                        	<div class="card__social">  
                            	<a class="share-icon facebook" href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php the_permalink(); ?>&p[title]=<?php the_title(); ?>&p[summary]=<?php the_excerpt(); ?>&p[images][0]=<?php get_the_post_thumbnail( $post_id, 'thumbnail' ); ?>"><span class="fa fa-facebook"></span></a>
                            	<a class="share-icon twitter" href="http://twitter.com/home?status=Mira%20esta%20propuesta%20para%20San%20Isidro%20<?php the_permalink(); ?>"><span class="fa fa-twitter"></span></a>
                            	<a class="share-icon googleplus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><span class="fa fa-google-plus"></span></a>
                        	</div>

                        	<a id="share" class="share-toggle share-icon" href="#"></a>
                    	</div>

    					<div class="card__meta">
                        	<?php the_terms( get_the_ID(), 'tema', ' ', ', ',' ' ); ?>
                        	<?php the_time('j M', '<time>', '</time>'); ?>
    					</div>

    					<article class="card__article">
	    					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" ><?php the_title(); ?></a></h2>
	    				</article>
    				</div>
    			</div>
        		<?php }

        		wp_reset_postdata();
        		wp_reset_query();
			}

			die();
		}


		function save_colaboration($array_save) {

			if( !empty($array_save['title']) && !empty($array_save['desc']) ) {

				$array_post = array(
								'post_status'	=> 'pending',
  								'post_type'		=> 'proyecto',
  								'post_author'	=> get_current_user_id(),
  								'post_name'     => sanitize_title(esc_html($array_save['title'])),
  								'post_title'    => esc_html($array_save['title']),
  								'post_content'	=> esc_textarea($array_save['desc']),
  								'tax_input'		=> array( 'tema' => array(esc_html($array_save['cat'])) )
					);

				// Upload Post
				$this->the_post_id = wp_insert_post($array_post);

				if(is_wp_error($this->the_post_id)) return false;

				/*
				// Add Featured Image to Post
				$image_url  = 'http://s.wordpress.org/style/images/wp-header-logo.png'; // Define the image URL here
				$upload_dir = wp_upload_dir(); // Set upload folder
				$image_data = file_get_contents($image_url); // Get image data
				$filename   = basename($image_url); // Create image file name

				// Check folder permission and define file location
				if( wp_mkdir_p( $upload_dir['path'] ) ) {
    				$file = $upload_dir['path'] . '/' . $filename;
				} else {
    				$file = $upload_dir['basedir'] . '/' . $filename;
				}

				// Create the image  file on the server
				file_put_contents( $file, $image_data );

				// Check image file type
				$wp_filetype = wp_check_filetype( $filename, null );
				*/

				require_once( ABSPATH . 'wp-admin/includes/admin.php' );

				$file_return = wp_handle_upload( $array_save['img'], array('test_form' => false ) );

				if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) )
					return false;
				else {
					// checks the file type and stores in in a variable
            		$wp_filetype = wp_check_filetype( basename( $file_return['file'] ), null );

					// Set attachment data
					$attachment = array(
    					'post_mime_type' => $wp_filetype['type'],
    					//'post_title'     => sanitize_file_name( $filename ),
    					'post_title'	=> preg_replace('/.[^.]+$/', '', basename( $file_return['file'] ) ),
    					'post_content'   => '',
    					'post_status'    => 'inherit',
    					'guid' => $file_return['url']
					);

					// Create the attachment
					$attach_id = wp_insert_attachment( $attachment, $file_return['url'], $this->the_post_id );

					// Include image.php
					require_once(ABSPATH . 'wp-admin/includes/image.php');

					// Define attachment metadata
					$attach_data = wp_generate_attachment_metadata( $attach_id, $file_return['file'] );

					// Assign metadata to attachment
					wp_update_attachment_metadata( $attach_id, $attach_data );

					// And finally assign featured image to post
					set_post_thumbnail( $this->the_post_id, $attach_id );
				}

				return true;
			} else 
				return false;
		}


		/*****************************
			REGISTER FORM
		*****************************/
		function set_register_form() {
			echo '<p><label for="godni_text">Tu Nombre<br />
						<input class="textMsi" type="text" name="register_name" /></label></p>';

			// echo '<p><label for="godni_text">Tu Apellido<br />
			// 			<input class="textMsi" type="text" name="register_lastname" placeholder="Tu Apellido" /></label></p>';

			echo '<p><label for="godni_text">Tu Telefono<br />
						<input class="textMsi" type="text" name="register_telefono" /></label></p>';
		}

		function save_register_form($user_id, $password = "", $meta = array()) {

			if(isset($_POST['register_name']))
				add_user_meta($user_id, 'first_name', $_POST['register_name']);
				
			// if(isset($_POST['register_lastname']))
			// 	add_user_meta($user_id, 'last_name', $_POST['register_lastname']);

			if(isset($_POST['register_telefono']))
				add_user_meta($user_id, 'phone', $_POST['register_telefono']);
		}
	
		function error_register_form( $wp_error, $sanitized_user_login, $user_email ) {
			if(isset($wp_error->errors['empty_username'])){
       			unset($wp_error->errors['empty_username']);
    		}

    		if(isset($wp_error->errors['username_exists'])){
        		unset($wp_error->errors['username_exists']);
    		}

    		//update_option('pollo5',print_r($_POST,true));

    		if( !isset($_POST['register_name']) || empty($_POST['register_name']) )
    			 $wp_error->add( 'Nombre Error', '<strong>ERROR</strong>: Campo nombre esta vacio.' );

    		// if( !isset($_POST['register_lastname']) || empty($_POST['register_lastname']) )
    		// 	 $wp_error->add( 'Apellido Error', '<strong>ERROR</strong>: Campo apellido esta vacio.' );

    		if( !isset($_POST['register_telefono']) || empty($_POST['register_telefono']) )
    			 $wp_error->add( 'Telefono Error', '<strong>ERROR</strong>: Campo telefono esta vacio.' );
    		
    		return $wp_error;
		}

		function hack_register_form() {
			if(isset($_POST['user_login']) && isset($_POST['user_email']) && !empty($_POST['user_email']))
        		$_POST['user_login'] = $_POST['user_email'];
		}


		 function login_head(){

			echo '<style>
        			#registerform > p:first-child{
            			display:none;
        			}

        			body, html {
        				background : url("http://test.gopymes.pe/gobiernoabierto/wp-content/uploads/2016/03/fondo-2.jpg");
        			}

        			.login h1 a {
        				background : url("http://test.gopymes.pe/gobiernoabierto/wp-content/themes/canvas/assets/logo-gobabierto-wp-login.png");
        				width: 300px;
        				height: 120px;
        			}

        			.wp-core-ui .button-primary, .wp-core-ui .button-primary:hover {
        				background: #2ecc71;
        				border-color: #2ecc71 #2ecc71 #2ecc71;
        				text-shadow: 0 -1px 1px #2ecc71,1px 0 1px #2ecc71,0 1px 1px #2ecc71,-1px 0 1px #2ecc71;
        			}
    				</style>

    			<script type="text/javascript" src="<?php echo site_url("/wp-includes/js/jquery/jquery.js"); ?>"></script>
    			<script type="text/javascript">
        			jQuery(document).ready(function($){
            			$("#registerform > p:first-child").css("display", "none");
        			});
    			</script>
				});';
		}


		/***************
			Shortcode
		****************/
		function add_roboto($atts) {
			ob_start();

			global $post;

			if( isset($_POST['project_submit']) ) {

				$post_status = $_POST['project_parent']!=0?'teamwork':'publish';

				$array_post = array(
								'post_status'	=> $post_status,
  								'post_type'		=> 'project',
  								'post_author'	=> get_current_user_id(),
  								'post_name'     => sanitize_title(esc_html($_POST['project_title'])),
  								'post_title'    => esc_html($before_title.$_POST['project_title']),
  								'post_content'	=> esc_textarea($_POST['project_desc']),
  								'post_parent'	=> esc_html($_POST['project_parent']),
  								'tax_input'		=> array( 'topic' => array(esc_html($_POST['project_topic'])) )
					);
					
				$data_post = wp_insert_post($array_post);
				$target_path = wp_upload_dir();

				$img_path = $target_path['path'] .'/'. basename( $_FILES['project_image']['name']);
				$img_url = $target_path['url'] .'/'. basename( $_FILES['project_image']['name']);

				if(move_uploaded_file($_FILES['project_image']['tmp_name'], $img_path)) {

					// Check the type of file. We'll use this as the 'post_mime_type'.
					$filetype = wp_check_filetype( basename( $img_path ), null );

					// Prepare an array of post data for the attachment.
					$attachment = array(
						'guid'           => $target_path['url'] . '/' . basename( $img_path ), 
						'post_mime_type' => $filetype['type'],
						'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $img_path ) ),
						'post_content'   => '',
						'post_status'    => 'inherit'
					);

					// Insert the attachment.
					$attach_id = wp_insert_attachment( $attachment, $img_path, $data_post->ID );

					// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
					require_once( ABSPATH . 'wp-admin/includes/image.php' );

					// Generate the metadata for the attachment, and update the database record.
					$attach_data = wp_generate_attachment_metadata( $attach_id, $img_path );
					wp_update_attachment_metadata( $attach_id, $attach_data );

					set_post_thumbnail( $data_post, $attach_id );
				}

				add_post_meta($data_post->ID,'vote',0);

				echo '<div class="alert alert-success">Guardado correctamente : <a href="'.get_permalink($data_post->ID).'">Ver su propuesta</a></div>';
			}

			if( is_single() )
				$array_terms = wp_get_post_terms($post->ID,'topic');
			else
				$array_terms = get_terms( 'topic', array( 'hide_empty' => 0 ) );

			if( is_user_logged_in() )
				include_once dirname(__FILE__).'/templates/colaboration.php';
			else
				include_once dirname(__FILE__).'/templates/nologin.html';

			$output = ob_get_contents();
    		ob_end_clean();
    	
    		return $output;
		}
	}
?>